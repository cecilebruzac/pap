# Prêt à Pousser integration

[Try to respect Karma conventions for git commit Msg please](http://karma-runner.github.io/2.0/dev/git-commit-msg.html)


## Getting Started

Install Node.js and npm if they are not already on your machine.

Clone this repo into new project folder

Install the npm packages described in the package.json

```shell
npm install
```

## Available Scripts

In the project directory, you can run:

### `npm run build:css`

### `npm run watch-css`

Disable untreated lines from linter in scss partials --alt: #{$positive-color};

### `npm run sass-lint`

