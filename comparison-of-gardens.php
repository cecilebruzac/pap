<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<section class="comparison">
  <nav class="comparison__nav secondary-nav">
    <div class="row row--centered">
      <div class="secondary-nav__wrapper">
        <div class="secondary-nav__col secondary-nav__col--with-centered-content">
          <p class="menu__item">
            <a class="menu__link menu__link--with-icon"
               href="/modulo.php">
              <svg class="menu__link-icon"
                   viewBox="0 0 22 25" width="22" height="25"
                   role="img">
                <title>Modulo</title>
                <use xlink:href="assets/images/defs.svg#modulo"></use>
              </svg>
              <span class="text text--big">Modulo</span>
            </a>
          </p>
        </div><!--
        --><div class="secondary-nav__col">
          <p class="text text--big text--uppercase">vs.</p>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__col--with-centered-content">
          <p class="menu__item">
            <a class="menu__link menu__link--with-icon" href="/lilo.php">
              <svg class="menu__link-icon"
                   viewBox="0 0 18 21" width="18" height="21"
                   role="img">
                <title>Lilo</title>
                <use xlink:href="assets/images/defs.svg#lilo"></use>
              </svg>
              <span class="text text--big">Lilo</span>
            </a>
          </p>
        </div>
      </div>
    </div>
  </nav>
  <div class="comparison__content row row--centered">
    <div class="row__col row__col--centered row__col--three-fifths-md">
      <h1 class="comparison__title text text--bold text--centered text--big text--huge-md ">Quel potager d’intérieur vous
        correspond le&nbsp;plus&nbsp;?</h1>
    </div>
    <ul class="comparison__list">
      <li class="comparison__item row__col row__col--half">
        <article class="comparison__garden garden">
          <div class="garden__thumbnail">
            <img class="garden__image"
                 src="assets/images/modulo.png"
                 srcset="assets/images/modulo@2x.png 2x"
                 width="250"
                 alt="Modulo"/>
          </div>
          <h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">MODULO</h2>
          <h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre potager
            d’intérieur&nbsp;évolutif</h3>
          <p class="garden__see-more text text--medium">
            <a class="text__link" href="/modulo.php">Découvrir Modulo</a>
          </p>
        </article>
      </li><!--
      --><li class="comparison__item row__col row__col--half">
        <article class="comparison__garden garden">
          <div class="garden__thumbnail">
            <img class="garden__image"
                 src="assets/images/lilo.png"
                 srcset="assets/images/lilo@2x.png 2x"
                 width="250"
                 alt="Lilo"/>
          </div>
          <h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">LILO</h2>
          <h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre jardin à portée de&nbsp;main</h3>
          <p class="garden__see-more text text--medium">
            <a class="text__link" href="/lilo.php">Découvrir Lilo</a>
          </p>
        </article>
      </li>
    </ul>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">Qui a le plus de&nbsp;pots&nbsp;?</h2>
      <p class="text text--big text--centered">(sans jeu de mots)</p>
      <table class="comparison__table">
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 195 65" width="195" height="65"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#planter" x="0" y="0" width="45" height="65"></use>
                <use xlink:href="assets/images/defs.svg#planter" x="50" y="0" width="45" height="65"></use>
                <use xlink:href="assets/images/defs.svg#planter" x="100" y="0" width="45" height="65"></use>
                <use xlink:href="assets/images/defs.svg#planter" x="150" y="0" width="45" height="65"></use>
              </svg>
            </p>
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">4 pots</p>
            <p class="text text--medium text--big-sm text--bigger-lg">et 4 plantes incluses 🎁</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 195 65" width="195" height="65"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#planter" x="25" y="0" width="45" height="65"></use>
                <use xlink:href="assets/images/defs.svg#planter" x="75" y="0" width="45" height="65"></use>
                <use xlink:href="assets/images/defs.svg#planter" x="125" y="0" width="45" height="65"></use>
              </svg>
            </p>
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">3 pots</p>
            <p class="text text--medium text--big-sm text--bigger-lg">et 3 plantes incluses 🎁</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 46 56" width="46" height="56"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#special-planter"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg">
              <strong class="text--bold">Pots spéciaux</strong><br>pour plantes spéciales
            </p>
            <p class="comparison__alert text text--medium text--big-sm text--bigger-lg text--light text--uppercase">Sortie
              prévue fin 2018</p>
          </td>
          <td class="comparison__no-data comparison__table-cell"></td>
        </tr>
      </table>
    </article>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">Quelles plantes pouvez-vous&nbsp;cultiver&nbsp;?</h2>
      <table class="comparison__table">
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">38</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">aromates, mini-légumes et&nbsp;fleurs</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">38</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">aromates, mini-légumes et&nbsp;fleurs</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 80 60" width="80" height="60"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#lettuce"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Jusqu’à 4 laitues par&nbsp;Modulo</p>
            <p class="text text--medium text--big-sm">Cultivez jusqu’à 4 laitues en même temps. Vous pouvez ajouter autant
              de plantes que souhaité grâce à la modularité de&nbsp;Modulo.</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 80 60" width="80" height="60"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#lettuce"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">1 laitue max par&nbsp;Lilo</p>
            <p class="text text--medium text--big-sm">Lilo ne peut accueillir qu’une seule laitue au risque d’étouffer vos
              autres&nbsp;plantes.</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 78 61" width="78" height="61"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#tomato"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Compatible avec toutes les nouvelles
              variétés à&nbsp;venir</p>
          </td>
          <td class="comparison__no-data comparison__table-cell"></td>
        </tr>
      </table>
    </article>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">Où pouvez-vous mettre votre&nbsp;potager&nbsp;?</h2>
      <table class="comparison__table">
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 115 73" width="115" height="73"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#kitchen-bench" x="15" y="13" width="85" height="60"></use>
                <use xlink:href="assets/images/defs.svg#device" x="65" y="0" width="26" height="28"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Sur le comptoir de votre&nbsp;cuisine</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 115 73" width="115" height="73"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#kitchen-bench" x="15" y="13" width="85" height="60"></use>
                <use xlink:href="assets/images/defs.svg#device" x="65" y="0" width="26" height="28"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Sur le comptoir de votre&nbsp;cuisine</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 115 80" width="115" height="80"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#kitchen-bench" x="15" y="20" width="85" height="60"></use>
                <use xlink:href="assets/images/defs.svg#device" x="89" y="0" width="26" height="28"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Au mur</p>
          </td>
          <td class="comparison__no-data comparison__table-cell"></td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">8</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">pots en mode recto-verso</p>
            <p class="text text--medium text--big-sm">Cultivez 8 plantes en utilisant chaque face de Modulo. Pour cela,
              des pieds recto-verso et un luminaire supplémentaire sont&nbsp;nécessaires.</p>
          </td>
          <td class="comparison__no-data comparison__table-cell"></td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 34 72" width="34" height="72"
                   preserveAspectRatio="xMinYMin meet"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#device" x="0" y="1" width="34" height="37"></use>
                <use xlink:href="assets/images/defs.svg#device" x="0" y="35" width="34" height="37"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">L’un sur l’autre</p>
            <p class="text text--medium text--big-sm">Assemblez 2 Modulo l’un sur l’autre grâce aux entretoises vendues
              séparemment. Cette configuration est aussi possible en recto-verso pour ainsi cultiver un total de 16&nbsp;plantes&nbsp;!</p>
          </td>
          <td class="comparison__no-data comparison__table-cell"></td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 102 72" width="102" height="72"
                   preserveAspectRatio="xMinYMin meet"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#device" x="0" y="1" width="34" height="37"></use>
                <use xlink:href="assets/images/defs.svg#device" x="0" y="35" width="34" height="37"></use>
                <use xlink:href="assets/images/defs.svg#device" x="34" y="1" width="34" height="37"></use>
                <use xlink:href="assets/images/defs.svg#device" x="34" y="35" width="34" height="37"></use>
                <use xlink:href="assets/images/defs.svg#device" x="68" y="1" width="34" height="37"></use>
                <use xlink:href="assets/images/defs.svg#device" x="68" y="35" width="34" height="37"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Suspension murale infinie</p>
            <p class="text text--medium text--big-sm">Une accroche murale est livrée gratuitement avec Modulo pour le
              fixer où vous voulez. Il est possible de fixer autant de Modulo que souhaités dans cette configuration.
              Notre record à battre est de 6 Modulo, autant vous dire que ça en&nbsp;jette&nbsp;!</p>
          </td>
          <td class="comparison__no-data comparison__table-cell"></td>
        </tr>
      </table>
    </article>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">La lumière du soleil dans
        chaque&nbsp;potager</h2>
      <table class="comparison__table">
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">47</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">LEDs intégrées</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">37</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">LEDs intégrées</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 15 15" width="60" height="60"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#lamp-bulb"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Allumage automatique</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 15 15" width="60" height="60"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#lamp-bulb"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Allumage automatique</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 38 25" width="38" height="35"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#switch"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Bouton on/off</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 38 25" width="38" height="35"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#switch"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Bouton on/off</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 35 35" width="50" height="50"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#exposure"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Mode d’éclairage printemps/été</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 35 35" width="50" height="50"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#exposure"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Mode d’éclairage printemps/été</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 35 35" width="50" height="50"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#exposure" fill="rgba(155, 155, 155, 0.3)"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Réglage de l'intensité</p>
            <p class="text text--medium text--big-sm">Réglages des modes Printemps et Été</p>
          </td>
          <td class="comparison__table-cell">
            <p class="comparison__icon">
              <svg viewBox="0 0 35 35" width="50" height="50"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#exposure" fill="rgba(155, 155, 155, 0.3)"></use>
              </svg>
            </p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Réglage de l'intensité</p>
            <p class="text text--medium text--big-sm">Réglages des modes Printemps et Été</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">8W</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Consommation</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--big text--bigger-sm text--huge-lg text--bold">7W</p>
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Consommation</p>
          </td>
        </tr>
      </table>
    </article>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">La technologie
        hydroponique</h2>
      <table class="comparison__table">
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Brevet Float®</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Brevet Float®</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Capsules intégrant graines et&nbsp;nutriments</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Capsules intégrant graines et&nbsp;nutriments</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Capsules bio-dégradables</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Capsules bio-dégradables</p>
          </td>
        </tr>
      </table>
    </article>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">Accessoires et&nbsp;Appli</h2>
      <table class="comparison__table">
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Kit de rempotage</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Kit de rempotage</p>
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Pieds recto-verso</p>
          </td>
          <td class="comparison__no-data comparison__table-cell">
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Entretoise pour assemblage vertical</p>
          </td>
          <td class="comparison__no-data comparison__table-cell">
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">Attache murale</p>
          </td>
          <td class="comparison__no-data comparison__table-cell">
          </td>
        </tr>
        <tr>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">App gratuite 
              Prêt&nbsp;à&nbsp;Pousser</p>
          </td>
          <td class="comparison__table-cell">
            <p class="text text--medium text--big-sm text--bigger-lg text--bold">App gratuite 
              Prêt&nbsp;à&nbsp;Pousser</p>
          </td>
        </tr>
      </table>
    </article>
    <article class="comparison__article">
      <h2 class="comparison__title text text--bold text--centered text--big text--huge-md ">Alors, vous avez
        choisi&nbsp;?</h2>
      <ul class="comparison__list hide-md">
        <li class="comparison__item row__col row__col--half">
          <article class="comparison__garden garden">
            <div class="garden__thumbnail">
              <img class="garden__image"
                   src="assets/images/modulo.png"
                   srcset="assets/images/modulo@2x.png 2x"
                   width="250"
                   alt="Modulo"/>
            </div>
            <h3 class="garden__name text text--bigger text--bold">Modulo</h3>
            <p class="garden__intro text text--big">Le potager d’intérieur évolutif</p>
            <p class="garden__price text text--medium text--big-sm">149,95 €</p>

            <div class="garden__cta text--big">
              <button class="button button--sale">Acheter</button>
            </div>
            <p class="garden__see-more text text--medium">
              <a class="text__link" href="/modulo.php">Découvrir Modulo</a>
            </p>
          </article>
        </li><!--
        --><li class="comparison__item row__col row__col--half">
          <article class="comparison__garden garden">
            <div class="garden__thumbnail">
              <img class="garden__image"
                   src="assets/images/lilo.png"
                   srcset="assets/images/lilo@2x.png 2x"
                   width="250"
                   alt="Lilo"/>
            </div>
            <h3 class="garden__name text text--bigger text--bold">Lilo</h3>
            <p class="garden__intro text text--big">Le jardin à portée de main</p>
            <p class="garden__price text text--medium text--big-sm">99,95 €</p>
            <div class="garden__cta text--big">
              <button class="button button--sale">Acheter</button>
            </div>
            <p class="garden__see-more text text--medium">
              <a class="text__link" href="/lilo.php">Découvrir Lilo</a>
            </p>
          </article>
        </li>
      </ul>
      <ul class="comparison__products-list products-list list show-md">
        <li class="products-list__item row__col row__col--half-md row__col--third-lg">
          <article class="garden product">
            <div class="product__thumbnail">
              <img class="garden__image"
                   sizes="(min-width: 71.75em) 21.875em, calc(50vw - 1.875em)"
                   srcset="assets/images/modulo-355.jpg 355w,
                   assets/images/modulo-710.jpg 710w"
                   src="assets/images/modulo-355.jpg"
                   width="355"
                   alt="Modulo"/>
            </div>
            <div class="product__content">
              <h3 class="garden__name text text--big text--bigger-md text--bold">Modulo</h3>
              <p class="garden__intro text text--medium text--big-md">Le potager d’intérieur évolutif</p>
              <p class="garden__price text text--big text--bigger-md">149,95 €</p>

              <div class="garden__cta text--big">
                <button class="button button--sale">Acheter</button>
              </div>
              <p class="garden__see-more text text--medium">
                <a class="text__link" href="/modulo.php">Découvrir Modulo</a>
              </p>
            </div>
          </article>
        </li><!--
        --><li class="products-list__item row__col row__col--half-md row__col--third-lg">
          <article class="garden product">
            <div class="product__thumbnail">
              <img class="garden__image"
                   sizes="(min-width: 71.75em) 21.875em, calc(50vw - 1.875em)"
                   srcset="assets/images/lilo-355.jpg 355w,
                   assets/images/lilo-710.jpg 710w"
                   src="assets/images/lilo-355.jpg"
                   width="355"
                   alt="Lilo"/>
            </div>
            <div class="product__content">
              <h3 class="garden__name text text--big text--bigger-md text--bold">Lilo</h3>
              <p class="garden__intro text text--medium text--big-md">Le jardin à portée de main</p>
              <p class="garden__price text text--big text--bigger-md">99,95 €</p>
              <div class="garden__cta text--big">
                <button class="button button--sale">Acheter</button>
              </div>
              <p class="garden__see-more text text--medium">
                <a class="text__link" href="/lilo.php">Découvrir Lilo</a>
              </p>
            </div>
          </article>
        </li>
      </ul>
    </article>
  </div>
</section>

<hr>

<?php include 'components/footer.php'; ?>
