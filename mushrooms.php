<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<div>
	<nav class="category__nav secondary-nav">
		<div class="row row--centered">
			<div class="secondary-nav__wrapper">
				<div class="secondary-nav__col">
					<div class="secondary-nav__title-wrapper">
						<h1 class="text text--medium text--bigger-md text--bold">Kits à champignons</h1>
					</div><!--
					--><div class="secondary-nav__menu menu secondary-nav__show-md">
						<ul class="menu__list">
							<li class="menu__item">
								<a class="menu__link menu__link--active menu__link--active-border"
									 href="#">
									<span class="text text--big">Tous</span>
								</a>
							</li><!--
							--><li class="menu__item">
								<a class="menu__link"
									 href="#">
									<span class="text text--big">Pleurotes</span>
								</a>
							</li><!--
							--><li class="menu__item">
								<a class="menu__link"
									 href="#">
									<span class="text text--big">Pholiotes</span>
								</a>
							</li>
						</ul>
					</div>
				</div><!--
				--><div class="secondary-nav__col secondary-nav__show-md">
					<ul class="menu__list">
						<li class="menu__item">
							<a class="menu__link menu__link--with-icon"
								 href="#coffrets">
								<svg class="menu__link-icon" viewBox="0 0 20 20" width="20" height="20" role="img">
									<title>Modulo</title>
									<use xlink:href="assets/images/defs.svg#gift"></use>
								</svg>
								<span class="text text--big">Coffrets et accessoires</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="secondary-nav__col secondary-nav__hide-md">
					<button class="secondary-nav__toggler toggler"
									onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
									aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
										 role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
					</button>
				</div>
			</div>
			<div id="collapsable-secondary-menu"
					 class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
				<ul class="menu__list">
					<li class="menu__item">
						<a class="menu__link"
							 href="#">
							<span class="text text--big">Tous les kits</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link"
							 href="#">
							<span class="text text--big">Pleurotes</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link"
							 href="#">
							<span class="text text--big">Pholiotes</span>
						</a>
					</li>
					<li class="menu__item menu__item--with-stronger-separator">
						<a class="menu__link menu__link--with-icon"
							 href="#coffrets">
							<svg class="menu__link-icon" viewBox="0 0 20 20" width="20" height="20" role="img">
								<title>Modulo</title>
								<use xlink:href="assets/images/defs.svg#gift"></use>
							</svg>
							<span class="text text--big">Coffrets et accessoires</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<section class="category cf">
		<div class="category__intro">
			<div class="row row--centered">
				<div class="row__col row__col--centered row__col--three-fifths-md">
					<h2 class="text text--medium text--big-sm text--bigger-lg text--centered">Faites pousser de délicieux
						champignons BIO en 10 jours dans votre cuisine. Pour de vrai. Bon&nbsp;appétit&nbsp;👩‍🍳</h2>
				</div>
			</div>
			<img class="category__intro-ill"
					 sizes="100vw"
					 srcset="assets/images/kits-a-champignons-375.jpg 375w,
					 assets/images/kits-a-champignons-750.jpg 750w,
					 assets/images/kits-a-champignons-1024.jpg 1024w,
					 assets/images/kits-a-champignons-2048.jpg 2048w,
					 assets/images/kits-a-champignons-1366.jpg 1366w,
					 assets/images/kits-a-champignons-2732.jpg 2732w"
					 src="assets/images/kits-a-champignons-1024.jpg"
					 alt="Kits à champignons"/>
		</div>
		<div class="category__mushrooms">
			<div class="row row--centered">

			</div>
		</div>
	</section>
	<section class="section section--with-list section--with-alt-background-color">
		<div class="row row--centered">
			<ul class="section__list">
				<li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<img src="assets/images/union-europeenne-bio.svg"
								 alt="Union Européenne Bio"
								 height="50"/>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Certifié bio</h2>
					<p class="text text--medium text--centered">Les kits sont certifiés bio.<br>Les espèces sont&nbsp;naturelles.
					</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<img src="assets/images/produit-en-france.svg"
								 alt="Produit en France"
								 height="50"/>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Français</h2>
					<p class="text text--medium text--centered">Cocorico, ils sont fabriqués en France&nbsp;! En Normandie et en&nbsp;Bretagne.</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon section__icon--positive">
						<svg viewBox="0 0 100 70" width="70" height="50"
								 role="img">
							<use xlink:href="assets/images/defs.svg#plate"></use>
						</svg>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Délicieux</h2>
					<p class="text text--medium text--centered">Les champignons qui poussent des boites sont comestibles, bien&nbsp;sûr.</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<svg viewBox="0 0 70 100" width="35" height="50"
								 role="img">
							<use xlink:href="assets/images/defs.svg#mushroom"></use>
						</svg>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Pour 3-4 personnes</h2>
					<p class="text text--medium text--centered">Cuisinez une belle omelette ou un risotto pour 2 adultes et 2&nbsp;enfants.</p>
				</li>
			</ul>
		</div>
	</section>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Simple et rapide</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">10 jours, top&nbsp;chrono.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Tous nos kits poussent en 10 jours, dans votre cuisine (à température ambiante, avec de la lumière) et ils sont
					tous aussi simples à faire pousser les uns que les autres (arrosez une fois par jour et si vous oubliez, ce
					n'est pas grave, ils pousseront un peu plus&nbsp;lentement).
				</p>
				<p class="text text--medium text--big-md text--centered">
					Tous poussent une à trois fois selon l'humidité de la pièce, avec des récoltes suffisantes pour cuisiner un
					risotto, une persillade ou une omelette pour 3 à 4 personnes. La première pousse de tous les kits est&nbsp;garantie.
				</p>
			</div>
		</div>
	</section>
	<hr>
	<section class="section section--with-list">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Plusieurs variétés</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Comment choisir son kit
						champignon&nbsp;?</h2>
				</header>
			</div>
			<p class="text text--medium text--big-md text--centered">
				La différence entre ces champignons réside dans leur couleur (naturelle) et leur&nbsp;goût&nbsp;:
			</p>
			<ul class="section__list">
				<li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<img src="assets/images/pleurotes-gris.png"
								 srcset="assets/images/pleurotes-gris@2x.png 2x"
								 width="100"
								 alt="Pleurotes gris"/>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Pleurotes gris</h2>
					<p class="text text--medium text--centered">Grand classique de nos étals, légèrement&nbsp;sucré</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<img src="assets/images/pleurotes-jaunes.png"
								 srcset="assets/images/pleurotes-jaunes@2x.png 2x"
								 width="100"
								 alt="Pleurotes jaunes"/>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Pleurotes jaunes</h2>
					<p class="text text--medium text--centered">Odeur et goût de châtaigne ou&nbsp;noisette</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<img src="assets/images/pleurotes-roses.png"
								 srcset="assets/images/pleurotes-roses@2x.png 2x"
								 width="100"
								 alt="Pleurotes roses"/>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Pleurotes roses</h2>
					<p class="text text--medium text--centered">Plus charnu, légèrement boisé, réduit moins à la&nbsp;cuisson.</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--quarter-md">
					<p class="section__icon">
						<img src="assets/images/pholiotes.png"
								 srcset="assets/images/pholiotes@2x.png 2x"
								 width="100"
								 alt="Pholiotes"/>
					</p>
					<h2 class="text text--big text--bold text--uppercase text--centered">Pholiotes</h2>
					<p class="text text--medium text--centered">Texture du champignon de Paris et saveur d'un&nbsp;cèpe</p>
				</li>
			</ul>
		</div>
	</section>
	<img class="category__ill"
			 sizes="100vw"
			 srcset="assets/images/champignons-en-famille-375.jpg 375w,
					 assets/images/champignons-en-famille-750.jpg 750w,
					 assets/images/champignons-en-famille-1024.jpg 1024w,
					 assets/images/champignons-en-famille-2048.jpg 2048w,
					 assets/images/champignons-en-famille-1366.jpg 1366w,
					 assets/images/champignons-en-famille-2732.jpg 2732w"
			 src="assets/images/champignons-en-famille-1024.jpg"
			 alt="Champignons en famille"/>
	<section class="section section--with-alt-background-color">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="section__icon">
						<svg viewBox="0 0 100 100" width="100" height="100"
								 role="img">
							<use xlink:href="assets/images/defs.svg#mushroom-with-sparks"></use>
						</svg>
					</p>
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Magique et ludique</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Le cadeau idéal</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Ces drôles de boites à champignons vous font découvrir la magie du champignon, très simplement. Après avoir
					ouvert le kit de culture, les pleurotes poussent en une semaine et, lorsqu'ils pointent le bout de leur nez,
					doublent de taille chaque&nbsp;jour.
				</p>
				<p class="text text--medium text--big-md text--centered">
					Impressionnants et très, très, très ludiques, le kit champignon est un excellent cadeau pour tous les curieux,
					les fans de nature ou les adorateurs de pleurotes, champignons de Paris, cèpes, morilles et autres&nbsp;shiitakés&nbsp;!
				</p>
			</div>
		</div>
	</section>
	<section class="section ">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="section__icon section__icon--positive">
						<svg viewBox="0 0 100 70" width="100" height="70"
								 role="img">
							<use xlink:href="assets/images/defs.svg#plate"></use>
						</svg>
					</p>
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Frais et délicieux</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Le goût et la fraîcheur d’un champignon
						cueilli en forêt.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Bien meilleurs que ceux du commerce, les pleurotes de Prêt à Pousser égalent ceux des forêts grace à leur
					fraicheur leur conférant des saveurs prononcées. Pour déguster ces délicieux champignons, nous vous recommandons
					de les cuisiner en <a class="text__link" href="">risotto</a>, en <a class="text__link" href="">velouté</a>, en
					<a class="text__link" href="">omelette</a> ou en <a class="text__link" href="">persillade</a>. Retrouvez toutes
					<a class="text__link" href="">nos recettes pour cuisiner les champignons&nbsp;ici</a>.
				</p>
			</div>
		</div>
	</section>
	<section class="section section--with-alt-background-color">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Vraiment simple</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Cultivez vos champignons à la maison</h2>
				</header>
				<p class="video-iframe">
					<iframe title="Lilo"
									width="560"
									height="315"
									src="https://www.youtube.com/embed/Y0KCCTz-lCs"
									frameBorder="0"
									allow="autoplay; encrypted-media"
									allowFullScreen></iframe>
				</p>
				<p class="text text--medium text--big-md text--centered">
					Faire pousser des champignons maison n’a jamais été aussi simple&nbsp;! Il suffit d'arroser le kit de culture
					une fois par jour avec le spray (fourni gratuitement dans le coffret), avec de l'eau du robinet. Si vous
					oubliez, ce n'est pas grave, les champignons pousseront juste un peu moins&nbsp;vite.
				</p>
				<p class="text text--medium text--big-md text--centered">
					Pas besoin de jardin pour cultiver des champignons&nbsp;: laissez le kit à champignons dans votre cuisine, à
					température ambiante, avec un peu de&nbsp;lumière.
				</p>
				<p class="text text--medium text--big-md text--centered">
					Bien sûr, un mode d'emploi détaillé est présent dans chaque boite, de même que quelques&nbsp;recettes.
				</p>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="section__icon">
						<img class="section__icon-img"
								 src="assets/images/agriculture-biologique.svg"
								 alt="Agriculture Biologique"
								 height="100"/><!--
								 --><img class="section__icon-img"
												 src="assets/images/produit-en-france.svg"
												 alt="Produit en France"
												 height="100"/>
					</p>
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Naturel et français</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Le kit de culture de champignon bio</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Le substrat utilisé est constitué de paille et de son de blé en provenance de fermes de l'ouest de la France. Il
					est certifié Agriculture Biologique et pasteurisé pour garantir la qualité de chaque kit de culture de
					champignon. Dans des salles propres, le substrat est ensuite inoculé avec du mycélium de champignon puis laissé
					en incubation dans des sacs. Autrement dit, la graine de champignon est mélangée au substrat puis les sacs sont
					laissés au repos, dans un environnement contrôlé. Après quelques semaines, les sacs sont&nbsp;prêts.
				</p>
				<p class="text text--medium text--big-md text--centered">
					Nous avons utilisé pendant très longtemps du marc de café comme substrat, mais nous l'avons depuis remplacé par
					de la paille et du son de blé BIO. La culture des champignons sur paille a donc remplacé le champignon sur&nbsp;café&nbsp;!
				</p>
				<p class="text text--medium text--big-md text--centered">
					Cette technique est utilisée par les cultivateurs de champignons depuis plusieurs décennies pour garantir de
					belles récoltes et des champignons délicieux. La fabrication est réalisée en France, plus précisément en
					Bretagne et en Normandie. Les bureaux de Prêt à Pousser sont à&nbsp;Paris.
				</p>
			</div>
		</div>
	</section>

	<section class="section section--with-alt-background-color">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="section__icon">
						<img src="assets/images/kits-a-pleurotes.png"
								 srcset="assets/images/kits-a-pleurotes@2x.png 2x"
								 height="100"
								 alt="Pleurotes"/>
					</p>
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Pleurote&nbsp;:
						définition</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Le Pleurote, un champignon délicieux et
						simple à faire&nbsp;pousser.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					La culture des champignons doit être simple et le pleurote pousse dans toutes les conditions, ce qui permet
					d'avoir un kit champignon simple avec presque 100&#x202f;% de réussite. À l'inverse, un kit de culture de champignons de
					Paris est compliqué&nbsp;: il faut attendre trois semaines, contrôler l'humidité et ne pas dépasser une certaine
					température, ou faire sa culture de champignons dans la cave. Bref, nous ne recommandons pas la culture des
					champignons de Paris et le kit champignons de Paris n'est donc pas prêt d'arriver chez Prêt à Pousser. Nous
					travaillons cependant au développement de kit pour la culture de&nbsp;shiitaké.
				</p>
				<p class="text text--medium text--big-md text--centered">
					Avec Prêt à Pousser, faire pousser des champignons chez soi est simple&nbsp;: en 10 jours, sans condition
					d'humidité ou de température et pas besoin de jardin puisque le pleurote* pousse très bien en&nbsp;intérieur.
				</p>
				<p class="text text--medium text--big-md text--centered">
					Enfin, cela nous permet de proposer de faire découvrir le pleurote rose et le pleurote jaune, deux espèces
					excellentes à&nbsp;déguster.
				</p>
				<p class="text text--small text--centered">* eh oui, pleurote est un champignon masculin avec un t (et non
					pleurotte ou&nbsp;pleurottes)</p>
			</div>
		</div>
	</section>
</div>

<?php include 'components/footer.php'; ?>
