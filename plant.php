<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<div class="plant"
     id="description">
  <nav class="plant__nav secondary-nav">
    <div class="row row--centered">
      <div class="secondary-nav__wrapper">
        <div class="secondary-nav__col">
          <div class="secondary-nav__title-wrapper">
            <p class="secondary-nav__back">
              <a class="secondary-nav__back-link"
                 href="/plants.php">
                <svg viewBox="0 0 26 17" width="26" height="17"
                     role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#back"></use>
                </svg>
              </a>
            </p>
            <p class="text text--medium text--bigger-md text--bold">Basilic Thaï</p>
          </div><!--
          --><div class="secondary-nav__menu secondary-nav__show-md menu">
            <ul class="menu__list">
              <li class="menu__item">
                <a class="menu__link"
                   href="#description">
                  <span class="text text--big">Description</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#garantie-pousse">
                  <span class="text text--big">Garantie Pousse</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#coffrets">
                  <span class="text text--big">Coffrets</span>
                </a>
              </li>
            </ul>
          </div>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
          <button class="secondary-nav__toggler toggler"
                onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
                aria-label="Afficher ou masquer le sous menu">
            <span class="toggler__arrow">
              <svg viewBox="0 0 12 8" width="12" height="8"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#arrow-down"></use>
              </svg>
            </span>
          </button>
        </div><!--
        --><div class="secondary-nav__col">
          <form class="add-in-cart-form">
            <input type="hidden"
                   name="product-id" value="1">
            <div class="add-in-cart-form__quantity-input number-box">
              <input class="number-box__input"
                     title="product quantity"
                     name="product-quantity"
                     step="1" min="1" max="99" value="1"
                     type="number">
              <span class="number-box__step-up"
                    onclick="this.parentNode.querySelector('input[type=number]').stepUp()"></span>
              <span class="number-box__step-down"
                    onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></span>
            </div>
            <button class="add-in-cart-form__button button button--sale"
                    type="submit">Acheter
            </button>
          </form>
        </div>
      </div>
      <div id="collapsable-secondary-menu"
           class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link"
               href="#description">
              <span class="text text--big">Description</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#garantie-pousse">
              <span class="text text--big">Garantie Pousse</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#coffrets">
              <span class="text text--big">Coffrets</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="plant__presentation-and-description">
    <section class="plant__presentation row row--centered">
      <div class="plant__gallery-wrapper row__col row__col--three-fifths-md row__col--with-right-gutter-md show-md">
        <div class="plant__gallery gallery">
          <div class="gallery__scene"
               id="scene">
            <img class="gallery__photo"
                 src="https://placebear.com/630/630"
                 alt="Basilic Thaï"/>
          </div>
          <ul class="gallery__nav">
            <li class="gallery__item">
              <a href='https://placebear.com/630/630'
                 onclick="showPhotoInScene(event, this, 'scene')">
                <img src="https://placebear.com/100/100"
                     alt="Basilic Thaï"/>
              </a>
            </li><!--
            --><li class="gallery__item">
              <a href='https://placekitten.com/630/630'
                 onclick="showPhotoInScene(event, this, 'scene')">
                <img src="https://placekitten.com/100/100"
                     alt="Basilic Thaï"/>
              </a>
            </li>
          </ul>
        </div>
      </div><!--
      --><div class="plant__infos-wrapper row__col row__col--two-fifths-md row__col--with-left-gutter-md">
        <p class="plant__category text text--light">Aromate</p>
        <h1 class="plant__name text text--bigger text--huge-md text--bold">Basilic Thaï</h1>
        <p class="plant__intro text text--big text--bigger-md">Le meilleur basilic du&nbsp;monde</p>
        <div class="plant__price-wrapper">
          <p class="plant__price text text--big text--bigger-md">5,95&#x202f;€</p>
        </div>
        <div class="plant__slider slider hide-md">
          <div>
            <img src="https://placebear.com/777/777"
                 srcset="https://placebear.com/1554/1554 2x"
                 alt="Basilic Thaï"/>
          </div>
          <div>
            <img src="https://placebear.com/777/777"
                 srcset="https://placebear.com/1554/1554 2x"
                 alt="Basilic Thaï"/>
          </div>
          <div>
            <img src="https://placebear.com/777/777"
                 srcset="https://placebear.com/1554/1554 2x"
                 alt="Basilic Thaï"/>
          </div>
        </div>
        <p class="text text--medium">Le Basilic Thaï se distingue de son cousin le Grand Vert par son arôme anisé et
          épicé. Essayez-le&nbsp;!
        </p>
        <ul class="plant__caracteristics-list list list--style-disc">
          <li class="list__item text">1ère récolte au bout d’un&nbsp;mois</li>
          <li class="list__item text">Graine bio et française</li>
          <li class="list__item text">Capsule compostable et&nbsp;rempotable</li>
          <li class="list__item text">Compatible Lilo et&nbsp;Modulo</li>
        </ul>
        <ul class="plant__insurances-list list list--style-icon">
          <li class="plant__insurance list__item list__item--style-icon">
            <svg class="plant__insurance-icon list__icon"
                 viewBox="0 0 38 36" width="38" height="36"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#stock"></use>
            </svg>
            <span class="text text--medium">
              <strong class="text--bold">En stock</strong><br>Recevez votre Basilic à partir du Lundi <strong
                class="text--bold">18 Juin</strong>
            </span>
          </li>
          <li class="plant__insurance list__item list__item--style-icon">
            <svg class="plant__insurance-icon list__icon"
                 viewBox="0 0 35 36" width="35" height="36"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#gardener"></use>
            </svg>
            <span class="text text--medium">
              <strong class="text--bold">2431 apprentis jardiniers</strong><br>cultivent actuellement une plante dans un jardin d’intérieur Prêt à Pousser.
            </span>
          </li>
          <li class="plant__insurance list__item list__item--style-icon">
            <svg class="plant__insurance-icon list__icon"
                 viewBox="0 0 35 35" width="35" height="35"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#growth"></use>
            </svg>
            <span class="text text--medium">
              <strong class="text--bold">Garantie Pousse</strong><br>Ça va pousser, c’est satisfait ou échangé,
              <a class="text__link" href="">en savoir&nbsp;plus</a>.
            </span>
          </li>
        </ul>
      </div>
    </section>
    <section class="plant__description section"
             style="background-color: #FF7948">
      <div class="row row--centered">
        <div class="plant__description-content row__col row__col--three-fifths-md row__col--with-right-gutter-md">
          <header class="section__header">
            <p class="text text--big text--bigger-md text--uppercase text--light show-md">Description</p>
            <h2 class="text text--big text--bigger-md text--bold">Mais qui es-tu Basilic&nbsp;Thaï&nbsp;?</h2>
          </header>
          <p class="text text--medium text--big-md">Avec sa saveur légèrement anisée et piquante, le cousin asiatique du
            Grand Vert
            gagne à être connu. Habitué des cuisines thaï ou vietnamiennes, pourquoi ne pas le faire atterrir dans la
            vôtre grâce à notre gamme de potagers d’intérieur&nbsp;? Cuisiniers aventuriers, à
            vos&nbsp;fourneaux&nbsp;!</p>
        </div><!--
        --><div
            class="plant__illustration-sup-wrapper row__col row__col--two-fifths-md row__col--with-left-gutter-md show-md">
          <p class="plant__illustration-wrapper">
            <img class="plant__illustration"
                 src="assets/images/plants/basilic-grand-vert.png"
                 alt="Basilic grand vert"/>
          </p>
        </div>
      </div>
    </section>
  </div>
  <section class="section row row--centered">
    <div class="row__col row__col--three-fifths-md row__col--with-right-gutter-md">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light show-md">Détails de pousse</p>
        <h2 class="text text--big text--bigger-md text--bold">Ce qu’en pense notre ami&nbsp;jardinier</h2>
      </header>
      <p class="text text--medium text--big-md">Pour vous accompagner dans la pousse de vos plantes, téléchargez l’
        <a class="text__link" href="/mobile-app.php">application mobile Prêt à Pousser</a> et recevez des conseils de pousse
        personnalisés tout au long de la vie de vos&nbsp;pantes.
      </p>
    </div>
    <ul class="plant__advices-list list list--style-icon">
      <li class="list__item list__item--style-icon row__col row__col--half-sm row__col--third-md row__col--with-right-gutter-md">
        <svg class="list__icon"
             viewBox="0 0 30 30" width="30" height="30"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#germination"></use>
        </svg>
        <span class="text text--medium text--big-md">
          <strong class="text--bold">Temps moyen de germination</strong><br>2 à 5&nbsp;jours
        </span>
      </li><!--
      --><li class="list__item list__item--style-icon row__col row__col--half-sm row__col--third-md row__col--with-right-gutter-md">
        <svg class="list__icon"
             viewBox="0 0 30 30" width="30" height="30"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#harvest"></use>
        </svg>
        <span class="text text--medium text--big-md">
          <strong class="text--bold">Début de récolte</strong><br>4 à 6 semaines après&nbsp;semis
        </span>
      </li><!--
      --><li class="list__item list__item--style-icon row__col row__col--half-sm row__col--third-md row__col--with-right-gutter-md">
        <svg class="list__icon"
             viewBox="0 0 30 30" width="30" height="30"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#harvest-cycle"></use>
        </svg>
        <span class="text text--medium text--big-md">
          <strong class="text--bold">Durée de la récolte</strong><br>6 à 7 semaines avant le pesto&nbsp;final.
        </span>
      </li><!--
      --><li class="list__item list__item--style-icon row__col row__col--half-sm row__col--third-md row__col--with-right-gutter-md">
        <svg class="list__icon"
             viewBox="0 0 30 30" width="30" height="30"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#exposure"></use>
        </svg>
        <span class="text text--medium text--big-md">
          <strong class="text--bold">Mode d’exposition</strong><br>Mode «&nbsp;été&nbsp;»&nbsp;: 16h d’exposition
        </span>
      </li><!--
      --><li class="list__item list__item--style-icon row__col row__col--half-sm row__col--third-md row__col--with-right-gutter-md">
        <svg class="list__icon"
             viewBox="0 0 39 30" width="39" height="30"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#preservation"></use>
        </svg>
        <span class="text text--medium text--big-md">
          <strong class="text--bold">Mode de conservation</strong><br>Congeler entières les feuilles lavées et&nbsp;séchées
        </span>
      </li>
    </ul>
    <div class="row__col row__col--three-fifths-md row__col--with-right-gutter-md">
      <p class="text text--medium text--big-md">«&nbsp;Pour le récolter, coupez la tige principale au 2e nœud. Les
        fleurs de votre
        Basilic peut
        rendre son goût amer. Taillez-les si elles&nbsp;apparaissent.&nbsp;» —&nbsp;<b class="text--bold">Notre ami&nbsp;jardinier</b>
      </p>
    </div>
  </section>
  <section class="plant__recipe section">
    <div class="row row--centered">
      <div class="row__col row__col--two-fifths-md row__col--with-right-gutter-md">&nbsp;</div><!--
      --><div class="row__col row__col--three-fifths-md row__col--with-left-gutter-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light show-md">Recette</p>
          <h2 class="text text--big text--bigger-md text--bold">Le conseil du chef</h2>
        </header>
        <p class="text text--medium text--big-md">Prenez du poisson blanc, faites le griller/sauter/pocher/au four ou à
          la broche si
          vous y tenez, servez-le accompagné de Basilic Thaï ciselé, écorces et jus de citron, un peu
          de&nbsp;piment.</p>
        <p class="text text--medium text--big-md">Découvrir <a class="text__link" href="">les recettes pour toutes
          nos&nbsp;plantes</a>.</p>
      </div>
    </div>
  </section>

  <div id="garantie-pousse">
    <?php include 'components/content-sections/it-will-grow.php'; ?>
  </div>

  <?php include 'components/content-sections/it-s-natural.php'; ?>

  <section class="section section--with-gradient-background-color show-md">
    <div class="row row--centered">
      <div class="row__col row__col--two-fifths-md row__col--with-right-gutter-md">
        <p class="text text--medium text--centered">
          <img src="assets/images/dispositif.png"
               srcset="assets/images/dispositif@2x.png 2x"
               alt="Système hydroponique"
               width="100"/>
        </p>
      </div><!--
      --><div class="row__col row__col--three-fifths-md row__col--with-left-gutter-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light">La capsule</p>
          <h2 class="text text--big text--bigger-md text--bold">Un système hydroponique breveté et&nbsp;innovant.</h2>
        </header>
        <p class="text text--medium text--big-md">Per hoc minui studium suum existimans Paulus, ut erat in conplicandis
          negotiis
          artifex dirus, unde ei Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem
          ad sortem periculorum communium traxit.</p>
        <p class="text text--medium text--big-md">Découvrir <a class="text__link" href="/technology.php">la technologie Prêt à
          pousser</a>.</p>
      </div>
    </div>
  </section>
  <section class="section row row--centered"
           id="coffrets">
    <header class="section__header">
      <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Coffrets</p>
      <h2 class="text text--big text--bigger-md text--bold text--centered show-md">Le cadeau parfait. Même pour&nbsp;vous.</h2>
      <p class="text text--big text--bold text--centered hide-md">Les coffrets</p>
    </header>
    <div class="plant__relatve-boxes">
      <ul class="products-list list show-md">
        <li class="products-list__item row__col row__col--half-sm row__col--third-md">
          <article class="product">
            <div class="product__thumbnail">
              <a href="">
                <div class="product__image-wrapper">
                  <img class="product__image"
                       src="assets/images/coffret.png"
                       alt="Coffret - Les indispensables"/>
                </div>
              </a>
            </div>
            <div class="product__content">
              <header class="product__header product__designation-and-price">
                <div class="product__designation">
                  <p class="product__category text text--light">Coffret</p>
                  <h3 class="product__name text text--big text--bold">Les indispensables</h3>
                </div>
                <p class="product__price text text--big">15,95&#x202f;€</p>
              </header>
              <p class="product__intro text text--medium">Basilic Grand Vert, Menthe, Ciboulette et&nbsp;Thym</p>
              <form class="product__add-in-cart-form add-in-cart-form">
                <input type="hidden"
                       name="product-id" value="1">
                <input class="add-in-cart-form__quantity-input input"
                       type="number" name="product-quantity" step="1" min="1" max="99" value="1"/>
                <button class="add-in-cart-form__button button button--sale button--openwork"
                        type="submit">Acheter
                </button>
              </form>
              <p class="product__see-more text text--medium">
                <a class="product__see-more-link" href="">En savoir plus &rarr;</a>
              </p>
            </div>
          </article>
        </li><!--
        --><li class="products-list__item row__col row__col--half-sm row__col--third-md">
          <article class="product">
            <div class="product__thumbnail">
              <a href="/plant.php">
                <div class="product__image-wrapper">
                  <img class="product__image"
                       src="assets/images/coffret.png"
                       alt="Coffret - La Surprise du Chef"/>
                </div>
              </a>
            </div>
            <div class="product__content">
              <header class="product__header product__designation-and-price">
                <div class="product__designation">
                  <p class="product__category text text--light">Coffret</p>
                  <h3 class="product__name text text--big text--bold">La Surprise du Chef</h3>
                </div>
                <p class="product__price text text--big">15,95&#x202f;€</p>
              </header>
              <p class="product__intro text text--medium">Basilic Grand Vert, Menthe, Ciboulette et&nbsp;Thym</p>
              <form class="product__add-in-cart-form add-in-cart-form">
                <input type="hidden"
                       name="product-id" value="1">
                <div class="add-in-cart-form__quantity-input number-box">
                  <input class="number-box__input"
                         title="product quantity"
                         name="product-quantity"
                         step="1" min="1" max="99" value="1"
                         type="number">
                  <span class="number-box__step-up"
                        onclick="this.parentNode.querySelector('input[type=number]').stepUp()"></span>
                  <span class="number-box__step-down"
                        onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></span>
                </div>
                <button class="add-in-cart-form__button button button--sale button--openwork"
                        type="submit">Acheter
                </button>
              </form>
              <p class="product__see-more text text--medium">
                <a class="product__see-more-link" href="">En savoir plus &rarr;</a>
              </p>
            </div>
          </article>
        </li>
      </ul>
    </div>
    <div class="slider hide-md">
      <div class="product">
        <div class="product__thumbnail">
          <a href="">
            <div class="product__image-wrapper">
              <img class="product__image"
                   src="assets/images/coffret.png"
                   alt="Coffret - Les indispensables"/>
            </div>
          </a>
        </div>
        <div class="product__content">
          <header class="product__header product__designation-and-price">
            <div class="product__designation">
              <p class="product__name text text--big text--bold">Les indispensables</p>
            </div>
            <p class="product__price text text--big">15,95&#x202f;€</p>
          </header>
          <p class="product__intro text text--medium">Basilic Grand Vert, Menthe, Ciboulette et&nbsp;Thym</p>
          <form class="product__add-in-cart-form add-in-cart-form">
            <input type="hidden"
                   name="product-id" value="1">
            <button class="add-in-cart-form__button button button--sale button--openwork"
                    type="submit">Acheter
            </button>
          </form>
        </div>
      </div>
      <div class="product">
        <div class="product__thumbnail">
          <a href="/plant.php">
            <div class="product__image-wrapper">
              <img class="product__image"
                   src="assets/images/coffret.png"
                   alt="Coffret - La Surprise du Chef"/>
            </div>
          </a>
        </div>
        <div class="product__content">
          <header class="product__header product__designation-and-price">
            <div class="product__designation">
              <p class="product__name text text--big text--bold">La Surprise du Chef</p>
            </div>
            <p class="product__price text text--big">15,95&#x202f;€</p>
          </header>
          <p class="product__intro text text--medium">Basilic Grand Vert, Menthe, Ciboulette et&nbsp;Thym</p>
          <form class="product__add-in-cart-form add-in-cart-form">
            <input type="hidden"
                   name="product-id" value="1">
            <button class="add-in-cart-form__button button button--sale button--openwork"
                    type="submit">Acheter
            </button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <?php

  require_once 'components/content-sections/faq.php';

  $questions = array(
    array(
      "question" => "Pourquoi faut-il manger du Basilic&nbsp;Thaï&nbsp;?",
      "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
        Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
        traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
        percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
        potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
        rector ausus miserabiles casus levare multorum."
      ),
    array(
      "question" => "Comment se cuisine le Basilic&nbsp;Thaï&nbsp;?",
      "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
        Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
        traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
        percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
        potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
        rector ausus miserabiles casus levare multorum."
    ),
    array(
      "question" => "Combien de temps vit le Basilic&nbsp;Thaï&nbsp;?",
      "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
        Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
        traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
        percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
        potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
        rector ausus miserabiles casus levare multorum."
    )
  );

  displayFaq($questions);

  ?>

  <section class="section row row--centered">

    <header class="section__header">
      <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Compatibilité</p>
      <h2 class="text text--big text--bigger-md text--centered text--bold">À faire pousser dans nos potagers&nbsp;d’intérieur</h2>
    </header>
    <ul class="products-list list show-md">
      <li class="products-list__item row__col row__col--half-md row__col--third-lg">
        <article class="garden">
          <a class="garden__link"
             href="/lilo.php">
            <div class="garden__thumbnail">
              <img class="garden__image"
                   sizes="(min-width: 71.75em) 21.875em, calc(50vw - 1.875em)"
                   srcset="assets/images/lilo-355.jpg 355w,
                   assets/images/lilo-710.jpg 710w"
                   src="assets/images/lilo-355.jpg"
                   alt="Lilo"/>
            </div>
            <h3 class="garden__name text text--big text--bigger-md text--bold">Lilo</h3>
            <p class="garden__intro text text--medium text--big-md">Le jardin à portée de main</p>
          </a>
        </article>
      </li><!--
      --><li class="products-list__item row__col row__col--half-md row__col--third-lg">
        <article class="garden">
          <a class="garden__link"
             href="/modulo.php">
            <div class="garden__thumbnail">
              <img class="garden__image"
                   sizes="(min-width: 71.75em) 21.875em, calc(50vw - 1.875em)"
                   srcset="assets/images/modulo-355.jpg 355w,
                   assets/images/modulo-710.jpg 710w"
                   src="assets/images/modulo-355.jpg"
                   alt="Modulo"/>
            </div>
            <h3 class="garden__name text text--big text--bigger-md text--bold">Modulo</h3>
            <p class="garden__intro text text--medium text--big-md">Le potager d’intérieur évolutif</p>
          </a>
        </article>
      </li>
    </ul>
    <div class="slider hide-md">
      <div class="product">
        <div class="product__thumbnail">
          <a href="/lilo.php">
            <div class="product__image-wrapper product__image-wrapper--square">
              <picture>
                <source media="(max-width: 50em)"
                        sizes="calc(100vw - 1.25em)"
                        srcset="assets/images/lilo-355.jpg 355w,
												assets/images/lilo-710.jpg 710w,
												assets/images/lilo-780.jpg 780w,
												assets/images/lilo-1560.jpg 1560w">
                <img class="product__image"
                     src="assets/images/lilo-710.jpg"
                     alt="Lilo"/>
              </picture>
            </div>
          </a>
        </div>
        <div class="product__content">
          <p class="text text--medium text--centered"><strong class="text--bold text--big">Lilo</strong>, le jardin à
            portée de main
          </p>
        </div>
      </div>
      <div class="product">
        <div class="product__thumbnail">
          <a href="/modulo.php">
            <div class="product__image-wrapper product__image-wrapper--square">
							<picture>
								<source media="(max-width: 50em)"
												sizes="calc(100vw - 1.25em)"
												srcset="assets/images/modulo-355.jpg 355w,
												assets/images/modulo-710.jpg 710w,
												assets/images/modulo-780.jpg 780w,
												assets/images/modulo-1560.jpg 1560w">
								<img class="product__image"
										 src="assets/images/modulo-710.jpg"
										 alt="Modulo"/>
							</picture>
            </div>
          </a>
        </div>
        <div class="product__content">
          <p class="text text--medium text--centered"><strong class="text--bold text--big">Modulo</strong>, le potager
            d’intérieur évolutif</p>
        </div>
      </div>
    </div>
  </section>

  <section class="plant__list section section--with-alt-background-color">
    <div class="row row--centered">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Les plantes</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered">Découvrez les plantes qui se marient avec
          le Basilic&nbsp;Thaï</h2>
      </header>
      <?php include 'components/plants-list.php'; ?>
    </div>
  </section>
</div>

<?php include 'components/footer.php'; ?>
