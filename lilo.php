<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<div class="lilo"
     id="description">
  <nav class="lilo__nav secondary-nav">
    <div class="row row--centered">
      <div class="secondary-nav__wrapper">
        <div class="secondary-nav__col">
          <div class="secondary-nav__title-wrapper">
            <p class="text text--medium text--bigger-md text--bold">Lilo</p>
          </div><!--
          --><div class="secondary-nav__menu menu secondary-nav__show-md">
            <ul class="menu__list">
              <li class="menu__item">
                <a class="menu__link"
                   href="#description">
                  <span class="text text--big">Description</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#garantie-pousse">
                  <span class="text text--big">Garantie Pousse</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#plantes">
                  <span class="text text--big">Plantes</span>
                </a>
              </li>
            </ul>
          </div>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
          <button class="secondary-nav__toggler toggler"
                  onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
                  aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
                     role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
          </button>
        </div><!--
        --><div class="secondary-nav__col">
          <form class="add-in-cart-form">
            <input type="hidden"
                   name="product-id" value="1">
            <button class="add-in-cart-form__button button button--sale"
                    type="submit">Acheter
            </button>
          </form>
        </div>
      </div>
      <div id="collapsable-secondary-menu"
           class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link"
               href="#description">
              <span class="text text--big">Description</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#garantie-pousse">
              <span class="text text--big">Garantie Pousse</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#plantes">
              <span class="text text--big">Plantes</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section class="lilo__introduction">
    <picture class="show-md">
      <source media="(min-width: 50em)"
              sizes="100vw"
              srcset="assets/images/lilo-fullscreen-800.jpg 800w,
							assets/images/lilo-fullscreen-1024.jpg 1024w,
							assets/images/lilo-fullscreen-1600.jpg 1600w,
							assets/images/lilo-fullscreen-2048.jpg 2048w,
							assets/images/lilo-fullscreen-1366.jpg 1366w,
							assets/images/lilo-fullscreen-2732.jpg 2732w">
      <img class="lilo__illustration"
           src="assets/images/lilo-fullscreen-800.jpg"
           alt="Lilo"/>
    </picture>
    <div class="row row--centered">
      <article class="lilo__infos row__col row__col--third-md">
        <h1 class="text text--bigger text--huge-lg text--bold">Lilo</h1>
        <h2 class="text text--big text--bigger-md">Votre jardin à portée de&nbsp;main</h2>
        <div class="lilo__price-wrapper">
          <p class="lilo__price text text--big text--bigger-md hide-md">99,95&#x202f;€</p>
        </div>
        <div class="lilo__gallery slider hide-md">
          <div>
            <picture>
              <source media="(max-width: 50em)"
                      sizes="calc(100vw - 1.25em)"
                      srcset="assets/images/lilo-355.jpg 355w,
                        assets/images/lilo-710.jpg 710w,
                        assets/images/lilo-780.jpg 780w,
                        assets/images/lilo-1560.jpg 1560w">
              <img src="assets/images/lilo-710.jpg"
                   alt="Lilo"/>
            </picture>
          </div>
          <div>
            <picture>
              <source media="(max-width: 50em)"
                      sizes="calc(100vw - 1.25em)"
                      srcset="assets/images/potager-pour-tous-355.jpg 355w,
                        assets/images/potager-pour-tous-710.jpg 710w,
                        assets/images/potager-pour-tous-780.jpg 780w,
                        assets/images/potager-pour-tous-1560.jpg 1560w">
              <img src="assets/images/potager-pour-tous-355.jpg"
                   alt="Lilo et l'enfant"/>
            </picture>
          </div>
        </div>
        <p class="lilo__mention text text--medium hide-md">Faites pousser vos herbes aromatiques dans votre cuisine,
          toute l’année, en toute&nbsp;simplicité.</p>
        <p class="lilo__opinions-wrapper text">
          <svg class="lilo__opinions" viewBox="0 0 130 22" width="130" height="22"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#star" x="0" y="0" width="22" height="22"></use>
            <use xlink:href="assets/images/defs.svg#star" x="27" y="0" width="22" height="22"></use>
            <use xlink:href="assets/images/defs.svg#star" x="54" y="0" width="22" height="22"></use>
            <use xlink:href="assets/images/defs.svg#star" x="81" y="0" width="22" height="22"></use>
            <use xlink:href="assets/images/defs.svg#star" x="108" y="0" width="22" height="22"></use>
          </svg>
          <span class="lilo__link-to-opinions text"><a href="" class="text__link">voir les avis</a></span>
        </p>
        <p class="lilo__price text text--big-sm text--bigger-lg show-md">99,95&#x202f;€</p>
        <div class="lilo__cta text--big">
          <button class="button button--sale">Acheter</button>
        </div>
        <p class="lilo__mention text text--medium show-md">Faites pousser vos herbes aromatiques dans votre cuisine,
          toute l’année, en toute&nbsp;simplicité.</p>
      </article>
    </div>
  </section>

  <section class="lilo__insurances">
    <div class="row row--centered">
      <ul class="lilo__insurances-list list list--style-icon">
        <li class="plant__insurance list__item list__item--style-icon row__col row__col--third-md">
          <svg class="plant__insurance-icon list__icon"
               viewBox="0 0 38 36" width="38" height="36"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#stock"></use>
          </svg>
          <span class="text text--medium">
            <strong class="text--bold">En stock</strong><br>Recevez Lilo à partir du Lundi <strong class="text--bold">18 Juin</strong>
          </span>
        </li><!--
        --><li class="plant__insurance list__item list__item--style-icon row__col row__col--third-md">
          <svg class="plant__insurance-icon list__icon"
               viewBox="0 0 35 36" width="35" height="36"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#gardener"></use>
          </svg>
          <span class="text text--medium">
            <strong class="text--bold">2431 personnes</strong><br>utilisent un potager d’intérieur Prêt à Pousser en ce&nbsp;moment.
          </span>
        </li><!--
        --><li class="plant__insurance list__item list__item--style-icon row__col row__col--third-md">
          <svg class="plant__insurance-icon list__icon"
               viewBox="0 0 35 35" width="35" height="35"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#growth"></use>
          </svg>
          <span class="text text--medium">
            <strong class="text--bold">Garantie Pousse</strong><br>Ça va pousser, c’est satisfait ou&nbsp;échangé,
            <a class="text__link" href="">en&nbsp;savoir&nbsp;plus</a>.
          </span>
        </li>
      </ul>
    </div>
  </section>

  <hr class="hide-md">

  <section class="section lilo__video">
    <div class="row row--centered">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Vidéo</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered">Lilo en 51&nbsp;secondes</h2>
        <p class="text text--medium text--big-md text--centered">Juré, pas une de&nbsp;plus</p>
      </header>
      <div class="row__col row__col--three-fifths-md row__col--centered-md">
        <p class="video-iframe">
          <iframe title="Lilo"
                  width="560"
                  height="315"
                  src="https://www.youtube.com/embed/B9BB3yOzvIo"
                  frameBorder="0"
                  allow="autoplay; encrypted-media"
                  allowFullScreen></iframe>
        </p>
      </div>
    </div>
  </section>

  <?php
    require_once 'components/content-sections/farmers-opinions.php';

    $opinions = array(
      array(
        "author" => array(
                      "name" => "Arnaud",
                      "city" => "Lyon",
                      "country" => "France"
                    ),
        "value" => "5",
        "comment" => "J’ai acheté un Lilo avec un basilic Thaï, ça a super bien poussé pendant 3&nbsp;mois&nbsp;!"
      ),
      array(
        "author" => array(
                      "name" => "Marie",
                      "city" => "Lyon",
                      "country" => "France"
                    ),
        "value" => "5",
        "comment" => "Mon mari adore&nbsp;! Bon, moi, je préfère quand même le basilic classique grand&nbsp;vert."
      ),
      array(
        "author" => array(
                      "name" => "Jean-Luc",
                      "city" => "Lyon",
                      "country" => "France"
                    ),
        "value" => "4",
        "comment" => "Ma capsule n’a pas germé. On me l’a changée gratuitement et ensuite, ça a&nbsp;marché."
      ),
      array(
        "author" => array(
                      "name" => "Louise",
                      "city" => "Lyon",
                      "country" => "France"
                    ),
        "value" => "5",
        "comment" => "Première récolte en 3 semaines, ça pousse&nbsp;bien."
      ),
      array(
        "author" => array(
                      "name" => "Françoise",
                      "city" => "Lyon",
                      "country" => "France"
                    ),
        "value" => "5",
        "comment" => "On en a mis dans tous nos plats, c’est vraiment différent du basilic, rien à voir mais à&nbsp;essayer&nbsp;!"
      ),
    );

  $wrapperExtraClasses = 'show-md';

  displayFarmersOpinions($opinions, $wrapperExtraClasses);

  ?>

  <hr>

  <div id="garantie-pousse">
    <?php include 'components/content-sections/it-will-grow.php'; ?>
  </div>

  <?php include 'components/content-sections/it-s-natural.php'; ?>

  <section class="section lilo__gallery show-md">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Un potager pour tous</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Pour les grands et les&nbsp;petits</h2>
        </header>
        <div class="slider-with-arrows">
          <div>
            <img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
                 srcset="assets/images/potager-pour-tous-355.jpg 355w,
								 assets/images/potager-pour-tous-710.jpg 710w,
								 assets/images/potager-pour-tous-677.jpg 677w,
								 assets/images/potager-pour-tous-1354.jpg 1354w"
                 src="assets/images/potager-pour-tous-677.jpg"
                 width="677"
                 alt="Lilo et l'enfant"/>
          </div>
          <div>
            <img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
                 srcset="assets/images/potager-pour-tous-355.jpg 355w,
								 assets/images/potager-pour-tous-710.jpg 710w,
								 assets/images/potager-pour-tous-677.jpg 677w,
								 assets/images/potager-pour-tous-1354.jpg 1354w"
                 src="assets/images/potager-pour-tous-677.jpg"
                 width="677"
                 alt="Lilo et l'enfant"/>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php

    require_once 'components/content-sections/faq.php';

    $questions = array(
      array(
        "question" => "Pourquoi Lilo&nbsp;?",
        "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
          Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
          traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
          percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
          potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
          rector ausus miserabiles casus levare multorum."
      ),
      array(
        "question" => "Comment Lilo&nbsp;?",
        "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
          Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
          traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
          percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
          potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
          rector ausus miserabiles casus levare multorum."
      )
    );

  displayFaq($questions);

  ?>

  <section class="section lilo__light show-md">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Lumière performante et&nbsp;adaptable</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">La lumière du soleil dans votre potager&nbsp;d’intérieur.</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">Le luminaire s’allume automatiquement le matin et s‘éteint le soir. Équipé de dizaines de LEDs basse consommation, il sait reproduire la lumière du soleil pour répondre aux besoins de vos plantes. En savoir plus sur <a href="/technology.php" class="text__link">notre technologie</a>.</p>
      </div>
    </div>
    <div class="lilo__light-image-wrapper">
      <img sizes="100vw"
           srcset="assets/images/lilo-light-375.jpg 375w,
           assets/images/lilo-light-750.jpg 750w,
           assets/images/lilo-light-1024.jpg 1024w,
           assets/images/lilo-light-2048.jpg 2048w,
           assets/images/lilo-light-1366.jpg 1366w,
           assets/images/lilo-light-2732.jpg 2732w"
           src="assets/images/lilo-light-1024.jpg"
           alt="Lampe de Lilo"/>
    </div>
  </section>

  <div class="show-md">
    <?php include 'components/content-sections/simple-and-autonomous-device.php'; ?>
  </div>

  <hr>

  <section class="lilo__plants section"
           id="plantes">
    <div class="row row--centered">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Les plantes</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered">Plus de 40 variétés d’aromates, mini légumes et fleurs à cultiver.</h2>
      </header>
      <?php include 'components/plants-list.php'; ?>
    </div>
  </section>
</div>

<hr>

<?php include 'components/footer.php'; ?>

