<?php

require_once 'person.php';

$persons = array(
  array(
    "id" => "0",
		"slug" => "jean-pierre",
		"name" => "Jean-Pierre",
		"job" => "Directeur"
	),
	array(
		"id" => "1",
		"slug" => "jean-pierre",
		"name" => "Jean-Pierre",
		"job" => "Directeur"
	),
	array(
		"id" => "2",
		"slug" => "jean-pierre",
		"name" => "Jean-Pierre",
		"job" => "Directeur"
	),
	array(
		"id" => "3",
		"slug" => "jean-pierre",
		"name" => "Jean-Pierre",
		"job" => "Directeur"
	)
);

?>

<ul class="people list"><!--
  <?php foreach($persons as $item) { ?>
  --><li class="people__item row__col row__col--half-sm row__col--third-md row__col--quarter-lg">
		<?php displayPerson($item); ?>
	</li><!--
  <?php } ?>
--></ul>