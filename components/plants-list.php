<?php

$plants = array(
  array(
    "id" => "0",
    "slug" => "basilic-grand-vert",
    "name" => "Basilic grand&nbsp;vert",
    "category" => "Plante",
    "color" => "#65C1C3",
    "price" => "5,95",
    "intro" => "Parce que vos tomates le méritent."
  ),
  array(
    "id" => "1",
    "slug" => "pimprenelle",
    "name" => "Pimprenelle",
    "category" => "Plante",
    "color" => "#A3D8E9",
    "price" => "5,95",
    "intro" => "Parce que vos tomates le méritent."
  ),
  array(
    "id" => "2",
    "slug" => "menthe-marocaine",
    "name" => "Menthe Marocaine",
    "category" => "Plante",
    "color" => "#FBC772",
    "price" => "5,95",
    "intro" => "Parce que vos tomates le méritent."
  ),
  array(
    "id" => "3",
    "slug" => "sauge",
    "name" => "Sauge",
    "category" => "Plante",
    "color" => "#F19F67",
    "price" => "5,95",
    "intro" => "Parce que vos tomates le méritent."
  )
);

?>

<ul class="products-list products-list--plants list"><!--
  <?php foreach($plants as $item) { ?>
  --><li class="products-list__item row__col row__col--half-sm row__col--third-md row__col--quarter-lg">
    <article class="product">
      <div class="product__thumbnail">
        <div class="product__image-wrapper product__image-wrapper--square"
             style="background-color: <?php echo $item["color"]; ?>">
          <img class="product__image product__image--seed"
               src="assets/images/plants/<?php echo $item["slug"]; ?>.png"
               alt="<?php echo $item["name"]; ?>"/>
        </div>
        <a class="product__thumbnail-link"
           href="/plant.php"></a>
      </div>
      <div class="product__content">
        <header class="product__header">
          <p class="product__category text text--light show-md"><?php echo $item["category"]; ?></p>
          <div class="product__designation-and-price">
            <div class="product__designation">
              <h3 class="product__name text text--medium text--big-md text--bold"><?php echo $item["name"]; ?></h3>
            </div>
            <p class="product__price text text--big"><?php echo $item["price"]; ?>&#x202f;€</p>
          </div>
        </header>
        <p class="product__intro text text--medium show-md"><?php echo $item["intro"]; ?></p>
        <form class="product__add-in-cart-form add-in-cart-form">
          <input type="hidden"
                 name="product-id" value="<?php echo $item["id"]; ?>">
          <div class="add-in-cart-form__quantity-input number-box">
            <input class="number-box__input"
                   title="product quantity"
                   name="product-quantity"
                   step="1" min="1" max="99" value="1"
                   type="number">
            <span class="number-box__step-up"
                    onclick="this.parentNode.querySelector('input[type=number]').stepUp()"></span>
            <span class="number-box__step-down"
                  onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></span>
          </div>
          <button class="add-in-cart-form__button button button--sale button--openwork"
                  type="submit">Acheter
          </button>
        </form>
        <p class="product__see-more text text--medium show-md">
          <a class="product__see-more-link" href="/plant.php">En savoir plus &rarr;</a>
        </p>
      </div>
    </article>
  </li><!--
  <?php } ?>
--></ul>