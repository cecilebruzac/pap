<?php

$news = array(
  array(
    "id" => "0",
		"slug" => "modulo",
		"title" => "Notre nouvelle gamme de potagers d’intérieur avec l’arrivée de&nbsp;Modulo"
	),
	array(
		"id" => "1",
		"slug" => "modulo",
		"title" => "Visionnez et récupérez les liens Youtube de nos dernières&nbsp;vidéos."
	),
	array(
		"id" => "2",
		"slug" => "modulo",
		"title" => "Entrez dans le coeur de notre laboratoire Recherche et&nbsp;Développement."
	),
	array(
		"id" => "3",
		"slug" => "modulo",
		"title" => "Découvrez toute l’histoire de Pret à Pousser depuis ses débuts en&nbsp;2013."
	),
	array(
		"id" => "4",
		"slug" => "modulo",
		"title" => "Le design et les fonctions de la toute nouvelle App Pret à Pousser sont enfin&nbsp;révélés."
	)
);

?>

<ul class="news-list list"><!--
  <?php foreach($news as $item) { ?>
  --><li class="news-list__item row__col row__col--half-md">
		<article class="news">
			<a class="news__link"
				 href="/news/<?php echo $item["slug"];?>">
			<div class="news__image-wrapper">
				<img class="news__image"
						 src="https://placeimg.com/514/250/nature"
						 srcset="https://placeimg.com/1028/500/nature 2x"/>
			</div>
			<div class="news__content">
				<p class="text text--medium text--big-md text--bold text--centered">
					<?php echo $item["title"]; ?>
				</p>
			</div>
			</a>
		</article>
	</li><!--
  <?php } ?>
--></ul>