<?php function displayMainNav($isThereASecondaryNav = false){ ?>

  <nav class="nav <?php echo (!$isThereASecondaryNav) ? 'nav--with-shadow' : ''; ?>">
    <div class="nav__bar">
      <div class="nav__bar-row row row--centered">
        <a class="nav__brand" href="/" id="brand-to-reduce-on-scroll">
            <img class="nav__logo"
                 src="assets/images/pret-a-pousser.svg"
                 width="300"
                 height="125"
                 alt="Prêt à Pousser"/>
        </a><!--
        --><div class="nav__menus-wrapper">
          <div class="nav__menu-row" id="nav-row-to-hide-on-scroll">
            <p class="nav__promotion text text--bold">Livraison offerte dès 35&#x202f;€ - Satisfait
              ou&nbsp;échangé&nbsp;!</p>
            <div class="nav__menu menu">
              <ul class="menu__list">
                <li class="menu__item">
                  <a class="menu__link menu__link--with-icon" href="">
                    <svg class="menu__link-icon"
                         viewBox="0 0 15 15" width="20" height="20"
                         role="img" aria-hidden="true">
                      <use xlink:href="assets/images/defs.svg#magnifying-glass"></use>
                    </svg>
                    <span class="text text--medium">Recherche</span>
                  </a>
                </li><!--
                --><li class="menu__item">
                  <a class="menu__link menu__link--with-icon" href="">
                    <svg class="menu__link-icon"
                         viewBox="0 0 15 15" width="20" height="20"
                         role="img" aria-hidden="true">
                      <use xlink:href="assets/images/defs.svg#help"></use>
                    </svg>
                    <span class="text text--medium">Assistance</span>
                  </a>
                </li><!--
                --><li class="menu__item">
                  <a class="menu__link menu__link--with-icon" href="">
                    <svg class="menu__link-icon"
                         viewBox="0 0 15 15" width="20" height="20"
                         role="img" aria-hidden="true">
                      <use xlink:href="assets/images/defs.svg#user"></use>
                    </svg>
                    <span class="text text--medium">Mon compte</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="nav__menu-row nav__menu-row--with-basket">
            <div class="nav__menu menu">
              <ul class="menu__list">
                <li class="menu__item">
                  <a class="menu__link menu__link--with-icon" href="/gardens.php">
                    <svg class="menu__link-icon"
                         viewBox="0 0 15 15" width="20" height="20"
                         role="img" aria-hidden="true">
                      <use xlink:href="assets/images/defs.svg#lamp-bulb"></use>
                    </svg>
                    <span class="text text--big text--bold">Potagers d’intérieur</span>
                  </a>
                </li><!--
                --><li class="menu__item">
                  <a class="menu__link menu__link--with-icon" href="/plants.php">
                    <svg class="menu__link-icon"
                         viewBox="0 0 15 15" width="20" height="20"
                         role="img" aria-hidden="true">
                      <use xlink:href="assets/images/defs.svg#plant"></use>
                    </svg>
                    <span class="text text--big text--bold">Capsules de plantes</span>
                  </a>
                </li><!--
                --><li class="menu__item">
                  <a class="menu__link menu__link--with-icon" href="/mushrooms.php">
                    <svg class="menu__link-icon"
                         viewBox="0 0 100 100" width="20" height="20"
                         role="img" aria-hidden="true">
                      <use xlink:href="assets/images/defs.svg#mushroom-with-sparks"></use>
                    </svg>
                    <span class="text text--big text--bold">Kits à champignons</span>
                  </a>
                </li>
              </ul>
            </div>
            <button class="nav__basket-access basket-access"
                    aria-label="Voir le panier">
              <svg viewBox="0 0 34 35" width="34" height="35"
                   role="img">
                <title>Panier</title>
                <use xlink:href="assets/images/defs.svg#basket"></use>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </div>
  </nav>
  <nav class="mobile-nav <?php echo (!$isThereASecondaryNav) ? 'mobile-nav--with-shadow' : ''; ?>">
    <div class="mobile-nav__bar">
      <div class="mobile-nav__bar-row row row--centered">
        <button class="mobile-nav__toggler toggler"
                onclick="toogleMenu(this, 'collapsable-menu', true)"
                aria-label="Afficher ou masquer le menu">
        <span class="toggler__bars">
          <span class="toggler__bar"></span>
          <span class="toggler__bar"></span>
        </span>
        </button>
        <p class="mobile-nav__brand">
          <a href="/">
            <img class="mobile-nav__logo"
                 src="assets/images/pret-a-pousser.svg"
                 alt="Prêt à Pousser"/>
          </a>
        </p>
        <button class="mobile-nav__basket-access basket-access"
                aria-label="Voir le panier">
          <svg viewBox="0 0 24 25" width="24" height="25"
               role="img">
            <title>Panier</title>
            <use xlink:href="assets/images/defs.svg#basket"></use>
          </svg>
        </button>
      </div>
    </div>
    <div id="collapsable-menu" class="menu">
      <div class="menu__row row row--centered">
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link menu__link--with-icon" href="/gardens.php">
              <svg class="menu__link-icon"
                   viewBox="0 0 15 15" width="20" height="20"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#lamp-bulb"></use>
              </svg>
              <span class="text text--big">Potagers d’intérieur</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--with-icon" href="/plants.php">
              <svg class="menu__link-icon"
                   viewBox="0 0 15 15" width="20" height="20"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#plant"></use>
              </svg>
              <span class="text text--big">Capsules de plantes</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--with-icon" href="/mushrooms.php">
              <svg class="menu__link-icon"
                   viewBox="0 0 100 100" width="20" height="20"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#mushroom-with-sparks"></use>
              </svg>
              <span class="text text--big">Kits à champignons</span>
            </a>
          </li>
          <li class="menu__item menu__item--with-stronger-separator">
            <a class="menu__link menu__link--with-icon" href="">
              <svg class="menu__link-icon"
                   viewBox="0 0 15 15" width="20" height="20"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#team"></use>
              </svg>
              <span class="text text--big">L’équipe Prêt à Pousser</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--with-icon" href="">
              <svg class="menu__link-icon"
                   viewBox="0 0 15 15" width="20" height="20"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#help"></use>
              </svg>
              <span class="text text--big">Assistance</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--with-icon" href="">
              <svg class="menu__link-icon"
                   viewBox="0 0 15 15" width="20" height="20"
                   role="img" aria-hidden="true">
                <use xlink:href="assets/images/defs.svg#user"></use>
              </svg>
              <span class="text text--big">Mon compte</span>
            </a>
          </li>
        </ul>
        <form class="mobile-nav__search-form search-form"
              action=""
              method=""
              autocomplete="off"
              accept-charset="utf-8">
          <input class="search-form__input"
                 type="text"
                 name=""
                 value=""
                 placeholder="Menthe, Basilic"
                 autocomplete="off">
          <button class="search-form__button"
                  type="submit">
            <svg viewBox="0 0 15 15" width="20" height="20"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#magnifying-glass"></use>
            </svg>
          </button>
        </form>
      </div>
    </div>
  </nav>
  <ul class="main-notifications-list row row--centered <?php echo ($isThereASecondaryNav) ? 'main-notifications-list--after-secondary-menu' : ''; ?>">
    <li class="notification notification--special"
        id="notification-1">
      <p class="text text--medium text--bold">Une erreur est survenue.</p>
      <button class="notification__close-button"
              onclick="removeElement('notification-1');"
              aria-label="Effacer cette notification">
        <svg viewBox="0 0 15 15" width="15" height="15"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#close"></use>
        </svg>
      </button>
    </li>
  </ul>
  <ul class="cart-notifications-list row row--centered">
    <li class="notification notification--with-shadow"
        id="cart-notification-1">
      <p class="text text--medium text--bold">Votre sélection</p>
      <ul class="text text--medium">
        <li>1x Persil</li>
      </ul>
      <p class="text text--medium text--bold">🎁 Encore 3 capsules à&nbsp;sélectionner.</p>
      <button class="notification__cta button button--disabled"
              disabled>Valider
      </button>
      <button class="notification__close-button"
              onclick="removeElement('cart-notification-1')"
              aria-label="Effacer cette notification">
        <svg viewBox="0 0 15 15" width="15" height="15"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#close"></use>
        </svg>
      </button>
    </li>
    <li class="notification notification--with-shadow"
        id="cart-notification-2">
      <p class="text text--medium text--bold">Votre sélection</p>
      <ul class="text text--medium">
        <li>1x Persil</li>
        <li>2x Menthes Marocaines</li>
        <li>1x Bleuet</li>
      </ul>
      <button class="notification__cta button button--positive">Valider</button>
      <button class="notification__close-button"
              onclick="removeElement('cart-notification-2')"
              aria-label="Effacer cette notification">
        <svg viewBox="0 0 15 15" width="15" height="15"
             role="img" aria-hidden="true">
          <use xlink:href="assets/images/defs.svg#close"></use>
        </svg>
      </button>
    </li>
  </ul>
<?php } ?>