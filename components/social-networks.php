<ul class="social-networks">
	<li class="social-networks__item">
		<a href="">
			<img src="assets/images/instagram.svg" width="40" height="40" alt="instagram"/>
		</a>
	</li>
	<li class="social-networks__item">
		<a href="">
			<img src="assets/images/pinterest.svg" width="40" height="40" alt="pinterest"/>
		</a>
	</li>
	<li class="social-networks__item">
		<a href="">
			<img src="assets/images/facebook.svg" width="40" height="40" alt="facebook"/>
		</a>
	</li>
	<li class="social-networks__item">
		<a href="">
			<img src="assets/images/youtube.svg" width="40" height="40" alt="youtube"/>
		</a>
	</li>
	<li class="social-networks__item">
		<a href="">
			<img src="assets/images/twitter.svg" width="40" height="40" alt="twitter"/>
		</a>
	</li>
</ul>