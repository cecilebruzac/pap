<?php

$boxes = array(
  array(
    "id" => "0",
    "slug" => "les-indispensables",
    "name" => "Les indispensables",
    "category" => "Coffret",
    "price" => "15,95",
    "intro" => "Basilic Grand Vert, Menthe, Ciboulette et Thym"
  ),
  array(
    "id" => "1",
    "slug" => "la-surprise-du-chef",
    "name" => "La Surprise du Chef",
    "category" => "Coffret",
    "color" => "#A3D8E9",
    "price" => "15,95",
    "intro" => "Basilic Grand Vert, Menthe, Ciboulette et Thym"
  ),
  array(
    "id" => "2",
    "slug" => "apprenti-botaniste",
    "name" => "Apprenti Botaniste",
    "category" => "Coffret",
    "color" => "#FBC772",
    "price" => "15,95",
    "intro" => "Basilic Grand Vert, Menthe, Ciboulette et Thym"
  )
);

?>

<ul class="products-list list"><!--
  <?php foreach($boxes as $item) { ?>
  --><li class="products-list__item row__col row__col--half-md row__col--third-lg">
    <article class="product">
      <div class="product__thumbnail">
        <a href="">
          <div class="product__image-wrapper">
            <img class="product__image"
                 src="assets/images/coffret.png"
                 alt="Coffret - Les indispensables"/>
          </div>
        </a>
      </div>
      <div class="product__content">
        <header class="product__header product__designation-and-price">
          <div class="product__designation">
            <p class="product__category text text--light"><?php echo $item["category"]; ?></p>
            <h2 class="product__name text text--big text--bold"><?php echo $item["name"]; ?></h2>
          </div>
          <p class="product__price text text--big"><?php echo $item["price"]; ?>&#x202f;€</p>
        </header>
        <p class="product__intro text text--medium"><?php echo $item["intro"]; ?></p>
        <form class="product__add-in-cart-form add-in-cart-form">
          <input type="hidden"
                 name="product-id" value="<?php echo $item["id"]; ?>">
          <div class="add-in-cart-form__quantity-input number-box">
            <input class="number-box__input"
                   title="product quantity"
                   name="product-quantity"
                   step="1" min="1" max="99" value="1"
                   type="number">
            <span class="number-box__step-up"
                  onclick="this.parentNode.querySelector('input[type=number]').stepUp()"></span>
            <span class="number-box__step-down"
                  onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></span>
          </div>
          <button class="add-in-cart-form__button button button--sale button--openwork"
                  type="submit">Acheter
          </button>
        </form>
        <p class="product__see-more text text--medium">
          <a class="product__see-more-link" href="">En savoir plus &rarr;</a>
        </p>
      </div>
    </article>
  </li><!--
  <?php } ?>
--></ul>