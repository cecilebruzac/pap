<?php function displayPerson($data){ ?>
<article class="person">
	<div class="person__image-wrapper">
		<img class="person__image"
				 src="assets/images/trombinoscope/<?php echo $data["slug"]; ?>.jpg"
		alt="<?php echo $data["name"]; ?>"/>
	</div>
	<div class="person__content">
		<h3 class="text text--medium text--centered">
			<span class="text--big-md text--bold"><?php echo $data["name"]; ?></span>
			<br>
			<?php echo $data["job"]; ?>
		</h3>
		<?php if( $data["phone"] || $data["mail"]){ ?>
		<div class="person__contact">
			<p class="text text--medium text--centered">
				<?php if( $data["phone"]){ ?><a class="text__link" href="tel:+33142639995"><?php echo $data["phone"]; ?></a><?php } ?>
				<?php if( $data["phone"] && $data["mail"]){ ?><br><?php } ?>
				<?php if( $data["mail"]){ ?><a class="text__link" href="mailto:<?php echo $data["mail"]; ?>"><?php echo $data["mail"]; ?></a><?php } ?>
			</p>
		</div>
		<?php } ?>
	</div>
</article>
<?php } ?>
