<section class="section device">
  <div class="device__part row row--centered">
    <div class="row__col row__col--two-fifths-md row__col--with-right-gutter-md">
      <p class="text text--medium text--centered">
        <img class="device__ill"
             src="assets/images/capsule.png"
             srcset="assets/images/capsule@2x.png 2x"
             alt="La capsule"
             width="200"/>
      </p>
    </div><!--
    --><div class="row__col row__col--three-fifths-md row__col--with-left-gutter-md">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light show-md">Simple</p>
        <h2 class="text text--big text--bigger-md text--bold">La plante germe et grandit depuis la&nbsp;capsule.</h2>
      </header>
      <p class="text text--big-md">Cueillez-la toutes les semaines, et ce pendant 3 à 4 mois. En fin de pousse,
        insérez une nouvelle capsule pour découvrir une nouvelle plante. Mettez l'ancienne capsule au compost (elle
        est 100&#x202f;%&nbsp;biodégradable).</p>
    </div>
  </div>
  <div class="device__part row row--centered">
    <div class="row__col row__col--two-fifths-md row__col--with-right-gutter-md">
      <p class="text text--medium text--centered">
        <img class="device__ill"
             src="assets/images/float.png"
             srcset="assets/images/float@2x.png 2x"
             alt="La capsule"
             width="200"/>
      </p>
    </div><!--
    --><div class="row__col row__col--three-fifths-md row__col--with-left-gutter-md">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light show-md">Autonome</p>
        <h2 class="text text--big text--bigger-md text--bold">Un garde manger pour la&nbsp;plante</h2>
      </header>
      <p class="text text--big-md">Grâce à notre design breveté Float®, vous n’avez qu’une seule chose à faire&nbsp;:
        rajouter de l'eau une fois toutes les deux ou trois semaines, lorsque le flotteur descend. La plante y puise
        les sels minéraux naturels dont elle a besoin (ceux-ci sont libérés dans l'eau au fil des semaines). En savoir
        plus sur&nbsp;<a class="text__link" href="">l’hydroponie</a>.</p>
    </div>
  </div>
</section>