<?php function displayFarmersOpinions($opinions, $wrapperExtraClasses = ''){ ?>
<section class="section <?php echo $wrapperExtraClasses; ?>">
  <div class="row row--centered">
    <header class="section__header row__col row__col--centered row__col--half-md">
      <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Avis des cultivateurs</p>
      <h2 class="text text--big text--bigger-md text--bold text--centered">Ça a bien poussé chez&nbsp;eux</h2>
    </header>
    <ul class="opinions-list"><!--
      <?php foreach($opinions as $item) { ?>
      --><li class="opinions-list__item row__col row__col--half-sm row__col--quarter-md row__col--fifth-lg">
        <article class="opinion">
          <h3 class="opinion__author-name text text--medium text--big-md text--bold"><?php echo $item["author"]["name"]; ?></h3>
          <p class="opinion__origin text"><?php echo $item["author"]["city"]; ?>, <?php echo $item["author"]["country"]; ?></p>
          <p class="opinion__value text text--big-md">
            <svg viewBox="0 0 130 22" width="130" height="22"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#star" x="0" y="0" width="22" height="22"></use>
              <use xlink:href="assets/images/defs.svg#star" x="27" y="0" width="22" height="22"></use>
              <use xlink:href="assets/images/defs.svg#star" x="54" y="0" width="22" height="22"></use>
              <use xlink:href="assets/images/defs.svg#star" x="81" y="0" width="22" height="22"></use>
              <use xlink:href="assets/images/defs.svg#star" x="108" y="0" width="22" height="22"></use>
            </svg>
          </p>
          <p class="opinion__coment text text--big-md">«&nbsp;<?php echo $item["comment"]; ?>&nbsp;»</p>
        </article>
      </li><!--
      <?php } ?>
    --></ul>
    <p class="opinions-list__mention text">Avis datant du 6 Août 2018 et provenant d’utilisateurs Lilo 5 mois après leur achat.</p>
  </div>
</section>
<?php } ?>