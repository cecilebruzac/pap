<section class="section section--with-alt-background-color">
	<div class="row row--centered">
		<div class="row__col row__col--centered row__col--three-fifths-md">
			<header class="section__header">
				<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Naturel</p>
				<h2 class="text text--big text--bigger-md text--bold text--centered">Les graines sont BIO et françaises</h2>
			</header>
			<p class="section__icon text text--medium">
				<img class="section__icon-img"
						 src="assets/images/agriculture-biologique.svg"
						 alt="Agriculture Biologique"
						 height="100"/><!--
				--><img class="section__icon-img"
						 src="assets/images/produit-en-france.svg"
						 alt="Produit en France"
						 height="100"/>
			</p>
			<p class="text text--medium text--big-md text--centered">Les graines proviennent de semenciers français. Les graines d'aromates sont BIO et poussent dans de la tourbe et de la fibre de coco. Nos <a class="text__link" href="">capsules</a> sont en plastique d'origine végétale et sont biodégradables&nbsp;: en fin de pousse, vous pouvez donc les <a class="text__link" href="">rempoter</a> ou bien les mettre au&nbsp;compost.</p>
		</div>
	</div>
</section>