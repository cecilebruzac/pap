<?php function displayFaq($questions){ ?>
  <section class="section section--with-alt-background-color">
    <div class="row row--centered">
      <header class="section__header row__col row__col--centered row__col--half-md">
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Questions / Réponses</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered show-md">Vous pouvez répéter la&nbsp;question&nbsp;?</h2>
        <p  class="text text--big text--bold text--centered hide-md">Questions / Réponses</p>
      </header>
      <ul>
        <?php foreach($questions as $item) { ?>
        <li class="accordion">
          <div class="accordion__cta" role="button" tabindex="0">
            <h3 class="text text--medium text--big-md text--centered text--bold"><?php echo $item["question"]; ?></h3>
          </div>
          <div class="accordion__panel">
            <div class="row__col row__col--centered row__col--half-md">
              <p class="text text--medium text--centered"><?php echo $item["answer"]; ?></p>
            </div>
          </div>
        </li>
        <?php } ?>
      </ul>
      <div class="faq__punchline row__col row__col--centered row__col--half-md">
        <p class="text text--medium text--big-md text--centered text--bold">Vous avez une autre question&nbsp;?</p>
        <p class="text text--medium text--centered">Vous pouvez consulter notre FAQ ou directement contacter l’équipe Prêt à&nbsp;Pousser&nbsp;:</p>
        <p class="text text--medium text--centered"><a class="button" href="">Assistance</a></p>
      </div>
    </div>
  </section>
<?php } ?>
