<section class="section">
  <div class="row row--centered">
    <div class="row__col row__col--centered row__col--three-fifths-md">
      <header class="section__header">
        <p class="section__icon section__icon--positive">
          <svg viewBox="0 0 35 35" width="100" height="100"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#growth"></use>
          </svg>
        </p>
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Garantie pousse</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered show-md">Ça va pousser yéyé&nbsp;!</h2>
        <p class="text text--big text--bold text--centered hide-md">Garantie pousse</p>
      </header>
      <p class="text text--medium text--big-md text--centered">Lilo fournit tous les ingrédients pour vous garantir de belles
        plantes&nbsp;: eau, lumière, et nutriments. En cas de capsule capricieuse (cela peut arriver car ce sont des
        produits vivants), nous vous envoyons une nouvelle capsule illico presto&nbsp;! Vous pouvez faire un tour sur
        notre&nbsp;FAQ.</p>
    </div>
  </div>
</section>