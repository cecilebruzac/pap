<?php

$releases = array(
  array(
    "id" => "0",
		"date" => "06 septembre 2018",
		"titre" => "Lancement des pré-commandes Modulo",
		"link" => ""
	),
	array(
		"id" => "1",
		"date" => "06 septembre 2018",
		"titre" => "Anniversaire de Kylian",
		"link" => ""
	)
);

?>

<ul class="releases"><!--
  <?php foreach($releases as $item) { ?>
  --><li class="releases__item release">
			<p class="release__col text text--medium text--bold"><?php echo $item["date"]; ?></p>
			<p class="release__col release__col--max text text--medium text--bold text--uppercase"><?php echo $item["titre"]; ?></p>
			<p class="release__download release__col text text--medium text--bold">
				<a class="release__download-link text__link" href="">Téléchargez</a>
			</p>
	</li><!--
  <?php } ?>
--></ul>

<button class="releases__see-more text text--medium text--centered">Voir les communiqué de presse plus anciens</button>