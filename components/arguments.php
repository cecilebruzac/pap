<section class="arguments section section--with-list">
	<div class="row row--centered">
		<ul class="section__list">
			<li class="arguments__item argument section__list-item row__col row__col--half row__col--quarter-md">
				<svg class="argument__icon argument__icon--red"
						 viewBox="0 0 59 52" width="59" height="52"
						 role="img" aria-hidden="true">
					<use xlink:href="assets/images/defs.svg#heart"></use>
				</svg>
				<br>
				<span class="argument__label text text--medium text--big-md text--bold">Satisfait ou échangé</span>
			</li><!--
			--><li class="arguments__item argument section__list-item row__col row__col--half row__col--quarter-md">
				<svg class="argument__icon argument__icon--green"
						 viewBox="0 0 36 52" width="36" height="52"
						 role="img" aria-hidden="true">
					<use xlink:href="assets/images/defs.svg#leaf"></use>
				</svg>
				<br>
				<span class="argument__label text text--medium text--big-md text--bold">100&#x202f;% naturel</span>
			</li><!--
			--><li class="arguments__item argument  section__list-item row__col row__col--half row__col--quarter-md">
				<svg class="argument__icon"
						 viewBox="0 0 48 52" width="48" height="52"
						 role="img" aria-hidden="true">
					<use xlink:href="assets/images/defs.svg#padlock"></use>
				</svg>
				<br>
				<span class="argument__label text text--medium text--big-md text--bold">Paiement sécurisé</span>
			</li><!--
			--><li class="arguments__item argument section__list-item row__col row__col--half row__col--quarter-md">
				<svg class="argument__icon argument__icon--blue"
						 viewBox="0 0 70 52" width="70" height="52"
						 role="img" aria-hidden="true">
					<use xlink:href="assets/images/defs.svg#shipping"></use>
				</svg>
				<br>
				<span class="argument__label text text--medium text--big-md text--bold">Livraison expresse</span>
			</li>
		</ul>
	</div>
</section>

