<?php include 'arguments.php'; ?>

<footer class="footer">
	<div class="row row--centered">
		<p class="footer__breadcrumb text text--medium"><a class="text__link" href="">Prêt à Pousser</a> > <a class="text__link" href="">Potagers d’intérieur</a> > <a class="text__link" href="">Capsules</a> > Basilic&nbsp;Thaï</p>
		<hr class="hide-lg">
		<ul class="hide-sm">
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Potagers d’intérieur</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="">Lilo</a></li>
						<li><a class="text__link" href="">Modulo</a></li>
						<li><a class="text__link" href="">Accessoires</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Capsules</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="">Aromates</a></li>
						<li><a class="text__link" href="">Mini-légumes</a></li>
						<li><a class="text__link" href="">Fleurs</a></li>
						<li><a class="text__link" href="">Laitues</a></li>
						<li><a class="text__link" href="">Coffrets</a></li>
						<li><a class="text__link" href="/subscription.php">Abonnements</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Kits à champignons</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="">Pleurotes gris</a></li>
						<li><a class="text__link" href="">Pleurotes jaunes</a></li>
						<li><a class="text__link" href="">Pleurotes roses</a></li>
						<li><a class="text__link" href="">Pholiotes</a></li>
						<li><a class="text__link" href="">Coffrets</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Prêt à Pousser</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="/team.php">Qui sommes-nous</a></li>
						<li><a class="text__link" href="/recruitment.php">Recrutement</a></li>
						<li><a class="text__link" href="/labo.php">Notre labo R&D</a></li>
						<li><a class="text__link" href="">Notre histoire</a></li>
						<li><a class="text__link" href="/school.php">Un Kit Une École</a></li>
						<li><a class="text__link" href="">Nos magasins</a></li>
						<li><a class="text__link" href="/technology.php">La technologie Prêt à&nbsp;Pousser</a></li>
						<li><a class="text__link" href="/mobile-app.php">L’app Prêt à Pousser</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Blog</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="">Actualités</a></li>
						<li><a class="text__link" href="">Recettes</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Presse</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="/newsroom.php">La Newsroom</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Mon compte</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="">Mes commandes</a></li>
						<li><a class="text__link" href="">Mes abonnements</a></li>
						<li><a class="text__link" href="">Mes infos perso</a></li>
						<li><a class="text__link" href="">Mes adresses</a></li>
						<li><a class="text__link" href="">Réductions et&nbsp;avoirs</a></li>
					</ul>
				</div>
			</li>
			<li class="accordion">
				<div class="accordion__cta" role="button" tabindex="0">
					<p class="text text--medium text--bold">Assistance</p>
				</div>
				<div class="accordion__panel">
					<ul class="text text--medium">
						<li><a class="text__link" href="">FAQ</a></li>
						<li><a class="text__link" href="tel:+33142639995">01 42 63 99 95</a></li>
						<li><a class="text__link" href="mailto:&#098;&#111;&#110;&#106;&#111;&#117;&#114;&#064;&#112;&#114;&#101;&#116;&#097;&#112;&#111;&#117;&#115;&#115;&#101;&#114;&#046;&#102;&#114;">&#098;&#111;&#110;&#106;&#111;&#117;&#114;&#064;&#112;&#114;&#101;&#116;&#097;&#112;&#111;&#117;&#115;&#115;&#101;&#114;&#046;&#102;&#114;</a></li>
					</ul>
				</div>
			</li>
		</ul>
		<ul class="footer__menu row__col show-sm row__col--three-quarters-lg">
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Potagers d’intérieur</a>
				</span>
				<ul>
					<li><a class="text__link" href="">Lilo</a></li>
					<li><a class="text__link" href="">Modulo</a></li>
					<li><a class="text__link" href="">Accessoires</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Capsules</a>
				</span>
				<ul>
					<li><a class="text__link" href="">Aromates</a></li>
					<li><a class="text__link" href="">Mini-légumes</a></li>
					<li><a class="text__link" href="">Fleurs</a></li>
					<li><a class="text__link" href="">Laitues</a></li>
					<li><a class="text__link" href="">Coffrets</a></li>
					<li><a class="text__link" href="/subscription.php">Abonnements</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Kits à champignons</a>
				</span>
				<ul>
					<li><a class="text__link" href="">Pleurotes gris</a></li>
					<li><a class="text__link" href="">Pleurotes jaunes</a></li>
					<li><a class="text__link" href="">Pleurotes roses</a></li>
					<li><a class="text__link" href="">Pholiotes</a></li>
					<li><a class="text__link" href="">Coffrets</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Prêt à Pousser</a>
				</span>
				<ul>
					<li><a class="text__link" href="/team.php">Qui sommes-nous</a></li>
					<li><a class="text__link" href="/recruitment.php">Recrutement</a></li>
					<li><a class="text__link" href="/labo.php">Notre labo R&D</a></li>
					<li><a class="text__link" href="">Notre histoire</a></li>
					<li><a class="text__link" href="/school.php">Un Kit Une École</a></li>
					<li><a class="text__link" href="">Nos magasins</a></li>
					<li><a class="text__link" href="/technology.php">La technologie Prêt à&nbsp;Pousser</a></li>
					<li><a class="text__link" href="/mobile-app.php">L’app Prêt à Pousser</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Blog</a>
				</span>
				<ul>
					<li><a class="text__link" href="">Actualités</a></li>
					<li><a class="text__link" href="">Recettes</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Presse</a>
				</span>
				<ul>
					<li><a class="text__link" href="/newsroom.php">La Newsroom</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Mon compte</a>
				</span>
				<ul>
					<li><a class="text__link" href="">Mes commandes</a></li>
					<li><a class="text__link" href="">Mes abonnements</a></li>
					<li><a class="text__link" href="">Mes infos perso</a></li>
					<li><a class="text__link" href="">Mes adresses</a></li>
					<li><a class="text__link" href="">Réductions et&nbsp;avoirs</a></li>
				</ul>
			</li>
			<li class="footer__menu-group text text--medium">
				<span class="text--bold">
					<a class="text__link"
						 href="">Assistance</a>
				</span>
				<ul>
					<li><a class="text__link" href="">FAQ</a></li>
					<li><a class="text__link" href="tel:+33142639995">01 42 63 99 95</a></li>
					<li><a class="text__link" href="mailto:&#098;&#111;&#110;&#106;&#111;&#117;&#114;&#064;&#112;&#114;&#101;&#116;&#097;&#112;&#111;&#117;&#115;&#115;&#101;&#114;&#046;&#102;&#114;">&#098;&#111;&#110;&#106;&#111;&#117;&#114;&#064;&#112;&#114;&#101;&#116;&#097;&#112;&#111;&#117;&#115;&#115;&#101;&#114;&#046;&#102;&#114;</a></li>
				</ul>
			</li>
		</ul><!--
		--><hr class="show-sm hide-lg"><!--
  	--><div class="row__col row__col--quarter-lg">
			<div class="footer__subscibe-to-newsletter">
				<p class="text text--medium text--bold">La newsletter trop&nbsp;cool</p>
				<form class="footer__subscibe-to-newsletter-form subscibe-to-newsletter-form">
					<input type="text" name="subscribe" class="input" placeholder="Mon email ici"/>
					<button class="button button--positive"
									type="submit">Ok
					</button>
				</form>
				<p class="text text--medium">dans laquelle on ne raconte pas de&nbsp;salades<br><span class="text--grey">(on était obligé de la faire&nbsp;celle-là)</span></p>
			</div>
			<hr class="hide-lg">
			<div class="footer__social-networks">
				<p class="text text--medium text--bold">Les rézossossios</p>
				<?php include 'social-networks.php'; ?>
				<p class="text text--medium">Allez, ramène ta fraise&nbsp;!<br><span class="text--grey">(ok, on&nbsp;arrête)</span></p>
			</div>
		</div>
		<hr>
		<div class="footer__legals-and-coutry">
			<div class="footer__legals">
				<p class="text text--medium text--grey">©Prêt à Pousser — Tous droits réservés</p>
				<ul class="footer__legals-menu legals-menu">
					<li class="legals-menu__item text text--medium"><a class="text__link" href="">Politique de protection des données personnelles</a></li>
					<li class="legals-menu__item text text--medium"><a class="text__link" href="">CGV</a></li>
					<li class="legals-menu__item text text--medium"><a class="text__link" href="">Mentions légales</a></li>
					<li class="legals-menu__item text text--medium"><a class="text__link" href="">Plan du site</a></li>
				</ul>
			</div>
			<hr class="hide-lg">
			<div class="footer__country">
				<p class="text text--medium">Changer de pays&nbsp;:
					<select class="footer__select-country text text--medium"
									name="country"
									required>
						<option value="france">🇫🇷 France</option>
						<option value="deutschland">🇩🇪 Deutschland</option>
					</select>
				</p>
			</div>
		</div>
	</div>
</footer>

<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="node_modules/slick-carousel/slick/slick.min.js"></script>
<script src="assets/scripts/main.js"></script>
<script src="node_modules/svg4everybody/dist/svg4everybody.js"></script>
<script>svg4everybody();</script>
</body>
</html>
