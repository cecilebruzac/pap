<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  displayMainNav();
?>

<div class="technology">
	<section class="technology__intro">
		<div class="row row--centered">
			<div class="row__col row__col--half-sm row__col--third-md">
				<h1 class="text text--bold text--big text--huge-md">La technologie Prêt&nbsp;à&nbsp;Pousser</h1>
				<p class="text text--big text--bigger-md">Grâce à notre système de potager d’intérieur révolutionnaire,
					l’hydroponie devient simple, abordable et&nbsp;accessible.</p>
			</div>
		</div>
		<img class="technology__intro-ill"
				 sizes="100vw"
				 srcset="assets/images/modulo-3d-375.jpg 375w,
				 assets/images/modulo-3d-750.jpg 750w,
				 assets/images/modulo-3d-1024.jpg 1024w,
				 assets/images/modulo-3d-2048.jpg 2048w,
				 assets/images/modulo-3d-1366.jpg 1366w,
				 assets/images/modulo-3d-2732.jpg 2732w"
				 src="assets/images/modulo-3d-1024.jpg"
				 alt="Modulo 3D"/>
	</section>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Culture
						hydroponique</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Un concentré de technologies breveté&nbsp;:
						Flo2at®</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Nous utilisons la culture hydroponique dans tous nos potagers d’intérieur&nbsp;: les plantes flottent et
					développent leur système racinaire dans l’eau. Afin d’améliorer les performances de nos jardins, nous avons
					créé et développé le système breveté Flo2at™. Il s’agit d’une culture sur flotteur pour optimiser
					l’oxygénation des racines. Ainsi, la plante respire mieux et puise une quantité plus importante des éléments
					qui lui sont indispensables et qui sont présents dans l’eau (dioxygène, minéraux et &nbsp;nutriments).
				</p>
			</div>
		</div>
	</section>
	<img class="technology__ill"
			 sizes="100vw"
			 srcset="assets/images/hydroponie-375.jpg 375w,
			 assets/images/hydroponie-750.jpg 750w,
			 assets/images/hydroponie-1024.jpg 1024w,
			 assets/images/hydroponie-2048.jpg 2048w,
			 assets/images/hydroponie-1366.jpg 1366w,
			 assets/images/hydroponie-2732.jpg 2732w"
			 src="assets/images/hydroponie-1024.jpg"
			 alt="culture hydroponique"/>
	<section class="technology__exposure section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="section__icon">
						<svg viewBox="0 0 35 35" width="100" height="100"
								 role="img" aria-hidden="true">
							<use xlink:href="assets/images/defs.svg#exposure"></use>
						</svg>
					</p>
					<p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Luminaire
						performant</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Et la lumière&nbsp;fut.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">Nous utilisons un luminaire comprenant des LEDs
					intégrées afin de reproduire la lumière du soleil. La lumière blanche produite est indispensable pour la
					photosynthèse des plantes et leur développement. Le spectre couvre toutes les longueurs d’ondes nécessaires.
					Les LEDs sont basse consommation et s’allument en fonction de la nature de vos plantes. Il existe un mode
					Printemps avec une intensité moindre et une photopériode de 12 heures et un mode Été de 16h pour les plantes
					estivales. La consommation en electricité est de 7 à 8&#x202f;W selon si vous possédez Lilo ou Modulo, ce qui
					équivaut à une consommation electrique deux fois moins importante que votre box internet, chapeau&nbsp;bas&nbsp;!</p>
			</div>
		</div>
	</section>
	<hr>
	<section class="technology__exposure section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="section__icon">
						<img src="assets/images/capsule.png"
								 srcset="assets/images/capsule@2x.png 2x"
								 alt="La capsule"
								 width="150"/>
					</p>
					<p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Capsule de
						plante</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">La capsule qui a tout pour&nbsp;elle</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">Nos capsules ont été pensées pour offrir aux plantes
					les meilleures conditions pour s’épanouir. Le substrat est composé de fibre de coco, de tourbe et de
					nutriments. Cela permet de réunir tous les ingrédients indispensables à une pousse réussie&nbsp;: excellente
					rétention en eau, PH optimal et richesse nutritive incroyable. Les capsules sont conçus de manière responsable
					en plastique végétal biocompostables, vous pouvez donc les&nbsp;composter.</p>
			</div>
		</div>
	</section>
</div>
<div class="gardens">
	<section class="gardens__introduction section">
		<div class="row row--centered show-md">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<p class="text text--big text--bigger-md text--uppercase text--light text--centered">Jardins d’appartement
					hydroponiques</p>
				<h2 class="text text--big text--bigger-md text--bold text--centered">Exigez le meilleur et commencez dès
					maintenant à cultiver vos plantes à la&nbsp;maison.</h2>
			</div>
		</div>
		<picture class="gardens__illustration">
			<source media="(min-width: 50em)"
							sizes="100vw"
							srcset="assets/images/potagers-800.jpg 800w,
							assets/images/potagers-1024.jpg 1024w,
							assets/images/potagers-1600.jpg 1600w,
							assets/images/potagers-2048.jpg 2048w,
							assets/images/potagers-1366.jpg 1366w,
							assets/images/potagers-2732.jpg 2732w">
			<img sizes="100vw"
					 srcset="assets/images/potagers-crop-375.jpg 375w,
					 assets/images/potagers-crop-750.jpg 750w,
					 assets/images/potagers-crop-800.jpg 800w,
					 assets/images/potagers-crop-1600.jpg 1600w"
					 src="assets/images/potagers-1024.jpg"
					 alt="Les potagers d'intérieur"/>
		</picture>
		<div class="row row--centered">
			<ul class="gardens__list">
				<li class="gardens__item row__col row__col--half-sm">
					<article class="gardens__garden garden">
						<a href="/modulo.php">
							<h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">MODULO</h2>
							<h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre potager d’intérieur&nbsp;évolutif</h3>
						</a>
						<p class="garden__price text text--big text--bigger-md">149,95 €</p>
						<div class="garden__cta text--big-md">
							<button class="button button--sale">Acheter</button>
						</div>
						<p class="garden__see-more text text--medium">
							<a class="text__link" href="/modulo.php">Découvrir Modulo</a>
						</p>
					</article>
				</li><!--
        --><li class="gardens__item row__col row__col--half-sm">
					<article class="gardens__garden garden">
						<a href="/lilo.php">
							<h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">LILO</h2>
							<h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre jardin à portée
								de&nbsp;main</h3>
						</a>
						<p class="garden__price text text--big text--bigger-md">99,95 €</p>
						<div class="garden__cta text--big-md">
							<button class="button button--sale">Acheter</button>
						</div>
						<p class="garden__see-more text text--medium">
							<a class="text__link" href="/lilo.php">Découvrir Lilo</a>
						</p>
					</article>
				</li>
			</ul>
		</div>
	</section>
</div>

<?php include 'components/footer.php'; ?>