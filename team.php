<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>
<div class="labo">
	<nav class="team__nav secondary-nav">
		<div class="row row--centered">
			<div class="secondary-nav__wrapper">
				<div class="secondary-nav__col">
					<p class="text text--medium text--bigger-md text--bold">Prêt à Pousser</p>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
					<button class="secondary-nav__toggler toggler"
									onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
									aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
										 role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
					</button>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__show-md">
					<div class="secondary-nav__menu menu">
						<ul class="menu__list">
							<li class="menu__item">
								<a class="menu__link menu__link--active menu__link--active-border"
									 href="/team.php">
									<span class="text text--big">Qui sommes-nous</span>
								</a>
							</li><!--
							--><li class="menu__item">
								<a class="menu__link" href="/recruitment.php">
									<span class="text text--big">Recrutement</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="/labo.php">
									<span class="text text--big">Notre labo R&D</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="#">
									<span class="text text--big">Notre histoire</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="/school.php">
									<span class="text text--big">1 Kit 1 École</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="collapsable-secondary-menu"
					 class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
				<ul class="menu__list">
					<li class="menu__item">
						<a class="menu__link menu__link--active"
							 href="/team.php">
							<span class="text text--big">Qui sommes-nous</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/recruitment.php">
							<span class="text text--big">Recrutement</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/labo.php">
							<span class="text text--big">Notre labo R&D</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="#">
							<span class="text text--big">Notre histoire</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/school.php">
							<span class="text text--big">1 Kit 1 École</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<header class="team__intro">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<h1 class="team__title text text--bold text--centered text--big text--huge-md">Bienvenue chez Prêt à&nbsp;Pousser</h1>
				<p class="text text--big text--bigger-md text--centered">Nos produits s’adressent à tous ceux qui de ceux qui
					désirent manger frais et prendre part à une petite révolution&nbsp;verte.</p>
			</div>
		</div>
	</header>
	<img class="team__ill"
			 sizes="100vw"
			 srcset="assets/images/team-375.jpg 375w,
			 assets/images/team-750.jpg 750w,
			 assets/images/team-1024.jpg 1024w,
			 assets/images/team-2048.jpg 2048w,
			 assets/images/team-1366.jpg 1366w,
			 assets/images/team-2732.jpg 2732w"
			 src="assets/images/team-1024.jpg"
			 alt="Équipe Prêt à Pousser"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Grâce à vous</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Prêt à Pousser est pionnier européen de
						la culture végétale à&nbsp;domicile.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Nous avons développé une technologie issue de l’hydroponie afin de rendre le potager d’intérieur accessible à
					tous. Aujourd’hui, plus de 70 000 apprentis jardiniers cultivent des herbes fraîches, petis légumes, tomates,
					laitues, fleurs et champignons. Pour ce faire, nous nous appuyons sur un réseau de formidables partenaires
					comptant plus de 300 magasins rien qu’en France. Merci à Nature & Découvertes, FNAC, Boulanger,&nbsp;Jardiland.
				</p>
			</div>
		</div>
	</section>
	<hr>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">L’équipe</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Nous sommes complètement fous de&nbsp;nature</h2>
				</header>
			</div>
			<div class="team__trombinoscope">
				<?php include 'components/people.php'; ?>
			</div>
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<p class="text text--medium text--big-md text--centered">
					<span class="texr--bold">Envie de nous rejoindre&nbsp;?</span><br>Découvrez <a class="text__link"
																																												 href="/recruitment.php">nos
					offres d’emploi du&nbsp;moment</a>.</p>
			</div>
		</div>
	</section>
	<hr>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Récompenses</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Prêt à Pousser est devenu l’entreprise leader en Europe dans le domaine de la pousse en&nbsp;intérieur.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Chez Prêt à Pousser, nous croyons aux bienfaits d’un contact renoué avec le végétal, pour des journées plus savoureuses et plus verte&nbsp;! Depuis 2013, notre jeune pousse travaille dur pour permettre à tout un chacun de faire pousser une partie de son assiette, directement chez&nbsp;soi.
				</p>
				<ul class="team__dates">
					<li class="team__date">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">2017</h3>
						<p class="text text--medium text--big-md text--centered">
							Déménagement dans Paris, dans le quartier des&nbsp;<a class="text__link" href="">Batignolles</a><br>
							Label <a class="text__link" href="">BPI</a>&nbsp;Excellence<br>
							Triplement de notre chiffre&nbsp;d’affaires
						</p>
					</li>
					<li class="team__date">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">2016</h3>
						<p class="text text--medium text--big-md text--centered">
							Label Observeur du&nbsp;Design<br>
							Trophée NPA des Objets&nbsp;Connectés<br>
							Prêt à Pousser membre du club restreint Smart Food&nbsp;Paris
						</p>
					</li>
					<li class="team__date">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">2015</h3>
						<p class="text text--medium text--big-md text--centered">
							Prix de l'audace et de l'inventivité du pavillon France à l'Exposition Universelle de&nbsp;Milan<br>
							1er Prix du concours Burda&nbsp;International
						</p>
					</li>
					<li class="team__date">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">2014</h3>
						<p class="text text--medium text--big-md text--centered">
							Grand Prix SIAL&nbsp;Innovation<br>
							Cervia – Prix Prêt à Manger, Practicité,&nbsp;Design<br>
							Petit Poucet – Prix Créateur&nbsp;d’Avenir<br>
							Réseau Entreprendre – Lauréat
						</p>
					</li>
					<li class="team__date">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">2013</h3>
						<p class="text text--medium text--big-md text--centered">
							ZeeEntrepreneurs – Finaliste<br>
							Cré’Acc – Coup de coeur du&nbsp;Jury
						</p>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<hr>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Notre concept</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Plus besoin d’avoir la main verte pour cultiver ses aromates, légumes et&nbsp;champignons.</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Chez vous, en intérieur et toute l’année, vous pouvez très simplement faire pousser tous nos produits&nbsp;gourmands.
				</p>
			</div>
		</div>
	</section>
	<picture class="team__ill">
		<source media="(min-width: 50em)"
						sizes="100vw"
						srcset="assets/images/potagers-800.jpg 800w,
							assets/images/potagers-1024.jpg 1024w,
							assets/images/potagers-1600.jpg 1600w,
							assets/images/potagers-2048.jpg 2048w,
							assets/images/potagers-1366.jpg 1366w,
							assets/images/potagers-2732.jpg 2732w">
		<img sizes="100vw"
				 srcset="assets/images/potagers-crop-375.jpg 375w,
					 assets/images/potagers-crop-750.jpg 750w,
					 assets/images/potagers-crop-800.jpg 800w,
					 assets/images/potagers-crop-1600.jpg 1600w"
				 src="assets/images/potagers-1024.jpg"
				 alt="Lilo ou Modulo"/>
	</picture>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<h2 class="text text--big text--bigger-md text--bold text--centered">Vous êtes plutôt Modulo ou&nbsp;Lilo&nbsp;?</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Découvrez notre gamme de potagers d’intérieur qui s’adapte à tous les intérieurs. Cultivez aromates, mini légumes, laitues, faites votre choix parmi plus de <a class="text__link" href="">40 variétés de&nbsp;plantes</a>&nbsp;!
				</p>
				<p class="text text--medium text--centered"><a class="button" href="/gardens.php">Potagers d’intérieur</a></p>
			</div>
		</div>
	</section>
	<img class="team__ill"
			 sizes="100vw"
			 srcset="assets/images/champignons-375.jpg 375w,
			 assets/images/champignons-750.jpg 750w,
			 assets/images/champignons-1024.jpg 1024w,
			 assets/images/champignons-2048.jpg 2048w,
			 assets/images/champignons-1366.jpg 1366w,
			 assets/images/champignons-2732.jpg 2732w"
			 src="assets/images/champignons-1024.jpg"
			 alt="Champignons"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<h2 class="text text--big text--bigger-md text--bold text--centered">Pleurotes jaunes, gris, roses ou bien&nbsp;pholiotes&nbsp;?</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Découvrez notre gamme de kits à champignons, il y en a pour tous les&nbsp;goûts.
				</p>
				<p class="text text--medium text--centered"><a class="button" href="/mushrooms.php">Kits à champignons</a></p>
			</div>
		</div>
	</section>
</div>
<hr>
<?php include 'components/footer.php'; ?>
