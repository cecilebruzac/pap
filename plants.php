<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<div>
  <nav class="category__nav secondary-nav">
    <div class="row row--centered">
      <div class="secondary-nav__wrapper">
        <div class="secondary-nav__col">
          <div class="secondary-nav__title-wrapper">
            <h1 class="text text--medium text--bigger-md text--bold">Plantes</h1>
          </div><!--
          --><div class="secondary-nav__menu menu secondary-nav__show-md">
            <ul class="menu__list">
              <li class="menu__item">
                <a class="menu__link menu__link--active menu__link--active-border"
                   href="#">
                  <span class="text text--big">Toutes</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#">
                  <span class="text text--big">Aromates</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#">
                  <span class="text text--big">Mini-légumes</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#">
                  <span class="text text--big">Fleurs</span>
                </a>
              </li>
            </ul>
          </div>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__show-md">
          <ul class="menu__list">
            <li class="menu__item">
              <a class="menu__link menu__link--with-icon"
                 href="#coffrets">
                <svg class="menu__link-icon" viewBox="0 0 20 20" width="20" height="20" role="img">
                  <title>Modulo</title>
                  <use xlink:href="assets/images/defs.svg#gift"></use>
                </svg>
                <span class="text text--big">Coffrets</span>
              </a>
            </li>
            <li class="menu__item">
              <a class="menu__link menu__link--with-icon"
                 href="/subscription.php">
                <svg class="menu__link-icon" viewBox="0 0 20 20" width="20" height="20" role="img">
                  <title>Modulo</title>
                  <use xlink:href="assets/images/defs.svg#calendar"></use>
                </svg>
                <span class="text text--big">Abonnement</span>
              </a>
            </li>
          </ul>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
          <button class="secondary-nav__toggler toggler"
                  onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
                  aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
                     role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
          </button>
        </div>
      </div>
      <div id="collapsable-secondary-menu"
           class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link"
               href="#">
              <span class="text text--big">Toutes les plantes</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#">
              <span class="text text--big">Aromates</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#">
              <span class="text text--big">Mini-légumes</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#">
              <span class="text text--big">Fleurs</span>
            </a>
          </li>
          <li class="menu__item menu__item--with-stronger-separator">
            <a class="menu__link menu__link--with-icon"
               href="#coffrets">
              <svg class="menu__link-icon" viewBox="0 0 20 20" width="20" height="20" role="img">
                <title>Modulo</title>
                <use xlink:href="assets/images/defs.svg#gift"></use>
              </svg>
              <span class="text text--big">Coffrets</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--with-icon"
               href="/subscription.php">
              <svg class="menu__link-icon" viewBox="0 0 20 20" width="20" height="20" role="img">
                <title>Modulo</title>
                <use xlink:href="assets/images/defs.svg#calendar"></use>
              </svg>
              <span class="text text--big">Abonnement</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <section class="category cf">
    <div class="category__search row row--centered">
      <form>
        <label class="input__wrapper hide-md">
          <input type="text" name="search" class="input input--with-icon" placeholder="Basilic, Menthe, etc." />
          <svg class="input__icon"
               viewBox="0 0 15 15" width="20" height="20"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#magnifying-glass"></use>
          </svg>
        </label>
      </form>
      <br class="hide-md">
      <select class="select"
              name="filter"
              required>
        <option value="" hidden>Filter</option>
        <option value="option-1">Option 1</option>
        <option value="option-1">Option 2</option>
      </select><!--
      --><select class="select"
              name="tri"
              required>
        <option value="" hidden>Tri</option>
        <option value="option-1">Option 1</option>
        <option value="option-1">Option 2</option>
      </select>
    </div>
    <div class="category__plants">
      <div class="row row--centered">
        <h2 class="hide">Toutes</h2>
        <?php include 'components/plants-list.php'; ?>
      </div>
    </div>
    <hr class="hide-md">
    <div id="coffrets"
         class="category__boxes row row--centered">
      <?php include 'components/boxes-list.php'; ?>
    </div>
  </section>
  <hr>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--medium text--big-md text--uppercase text--light text--sale text--centered">Nouveauté</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Abonnement</h2>
        </header>
        <p class="text text--big text--bigger-md text--centered">Recevez vos capsules à la fréquence de votre choix directement chez&nbsp;vous.</p>
        <p class="text text--medium text--centered"><a class="button" href="">Je découvre</a></p>
      </div>
    </div>
  </section>
  <div class="category__capsules">
    <picture>
      <source media="(max-width: 23.4375em)"
              sizes="100vw"
              srcset="assets/images/capsules-sm-375.jpg 375w,
  							assets/images/capsules-sm-750.jpg 750w">
      <source media="(max-width: 50em)"
              sizes="100vw"
              srcset="assets/images/capsules-md-800.jpg 800w,
  							assets/images/capsules-md-1600.jpg 1600w">
      <source media="(min-width: 50em)"
              sizes="100vw"
              srcset="assets/images/capsules-lg-1366.jpg 1366w,
  							assets/images/capsules-lg-2732.jpg 2732w">
      <img class="category__capsules-ill"
           src="assets/images/capsules-lg-1366.jpg"
           alt="Capsules"/>
    </picture>
  </div>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Une multitude de plantes</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">37 variétés pour vous plaire,<br>et bientôt&nbsp;plus&nbsp;!</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">
          Nous proposons un large choix de variétés de plantes 🌱 pour plaire à toutes et tous. Notre équipe R&D travaille continuellement sur l’arrivée de nouvelles variétés, alors n'hésitez pas à consulter <a class="text__link" href="">nos dernières actualités</a> pour être au courant des dernières innovations végétales. Nous avons plein de projets sous le coude, vous allez être surpris quand vous allez découvrir tout ce qu’il est possible de cultiver dans sa propre cuisine. Plus besoin d'installer un mini potager sur son balcon pour faire pousser ses&nbsp;aromates.
        </p>
      </div>
    </div>
  </section>
  <hr>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="section__icon">
            <svg viewBox="0 0 100 92" width="100" height="92"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#capsule"></use>
            </svg>
          </p>
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Capsules tout-en-un</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Cultivez en toute simplicité</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">
          Les capsules de plantes Prêt à Pousser contiennent tous les éléments pour réussir de magnifiques récoltes&nbsp;: graines BIO, substrat enrichi, tourbe et fibre de coco 🥥. Tout est inclus, vous n'avez rien à rajouter. Insérez simplement la capsule dans votre <a class="text__link" href="/gardens.php">jardin d'intérieur</a> Prêt à Pousser et remplissez la réserve d'eau. Vos plantes grandiront toutes seules comme par magie&nbsp;! Vous voyez, pas besoin d'avoir la main verte pour cultiver et déguster ses propres légumes, laitues et&nbsp;aromates&nbsp;!
        </p>
      </div>
    </div>
  </section>
  <hr>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="section__icon">
            <svg viewBox="0 0 100 126" width="100" height="126"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#big-lilo"></use>
            </svg>
          </p>
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Un design global</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">L'harmonie parfaite</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">
          Les capsules Prêt à Pousser sont compatibles avec <a class="text__link" href="/lilo.php">Lilo</a> et <a class="text__link" href="/modulo.php">Modulo</a>, nos jardins d'intérieur hydroponiques 💧. Notre équipe R&D a conçu ces capsules pour vous offrir le meilleur rendement et la meilleure pousse possible. Les capsules ont été pensées pour être cultivées dans nos potagers, et dans nos potagers seulement. Nous avons développé un design singulier et performant. Cette technologie a été protégée. Nous avons déposé le brevet flo2at®&nbsp;: il permet à tout le monde de cultiver ses propres aromates, légumes et fleurs directement chez soi, sans effort et tout au long de l'année. Vous pouvez en apprendre davantage sur <a class="text__link" href="">la technologie</a> Prêt à&nbsp;Pousser.
        </p>
      </div>
    </div>
  </section>
  <hr>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="section__icon">
            <svg viewBox="0 0 92 92" width="92" height="92"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#earth"></use>
            </svg>
          </p>
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Origine responsable</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Les graines sont bio et viennent de semenciers&nbsp;français</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">
          Toutes les graines incluses dans les capsules Prêt à Pousser sont bio&nbsp;! Nous nous approvisionnons directement auprès de semenciers français 🇫🇷, cela nous permet de contrôler l’origine et de vous garantir un niveau de qualité le plus élevé possible. Nous mettons à coeur de sélectionner nos partenaires et leurs graines selon des critères de performance et de responsabilité. Notre objectif est de délivrer un taux de pousse de 100&#x202f;%. Si toutefois, la plante est capricieuse, et c'est possible puisque l'on parle ici de produits vivants, alors nous vous remplacerons votre capsule sans frais et les plus rapidement possible. Nous avons appelé ce service gratuit la Garantie Pousse. Contactez-nous pour profiter de cet avantage sur <a class="text__link" href="">notre page Assistance</a>.
        </p>
      </div>
    </div>
  </section>
</div>
<hr>

<?php include 'components/footer.php'; ?>