<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  displayMainNav();
?>
<div class="newsroom">
	<header class="newsroom__intro">
		<div class="row row--centered">
			<h1 class="newsroom__title text text--bold text--centered text--big text--huge-md">Newsroom</h1>
			<p class="text text--big text--bigger-md text--centered">Bienvenue dans la newsroom de Prêt à Pousser. Découvrez
				nos derniers communiqués et contacts presse ainsi que toutes les informations et visuels de
				nos&nbsp;produits.</p>
		</div>
	</header>
	<img class="newsroom__ill"
			 sizes="100vw"
			 srcset="assets/images/laboratoire-crop-375.jpg 375w,
				 assets/images/laboratoire-crop-750.jpg 750w,
				 assets/images/laboratoire-crop-1024.jpg 1024w,
				 assets/images/laboratoire-crop-2048.jpg 2048w,
				 assets/images/laboratoire-crop-1366.jpg 1366w,
				 assets/images/laboratoire-crop-2732.jpg 2732w"
			 src="assets/images/laboratoire-crop-1024.jpg"
			 alt="Laboratoire"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Visuels</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Téléchargez tous nos&nbsp;visuels</h2>
					<p class="text text--medium text--big-md text--centered">Cliquez sur le bouton ci-dessous pour obtenir les
						visuels de tous nos produits ainsi que des photos de <a class="text__link" href="/team.php">l’équipe Prêt à
							Pousser</a> et son labo&nbsp;R&D&nbsp;!</p>
				</header>
				<p class="text text--medium text--centered"><a class="button" href="">Téléchargez les visuels</a></p>
			</div>
		</div>
	</section>
	<hr>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Communiqués de presse</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Découvrez nos derniers potagers, nos nouvelles plantes et nos super&nbsp;partenariats&nbsp;!</h2>
				</header>
				<?php include 'components/press-releases-list.php'; ?>
			</div>
		</div>
	</section>
	<img class="newsroom__ill"
			 sizes="100vw"
			 srcset="assets/images/controle-des-pots-375.jpg 375w,
				 assets/images/controle-des-pots-750.jpg 750w,
				 assets/images/controle-des-pots-1024.jpg 1024w,
				 assets/images/controle-des-pots-2048.jpg 2048w,
				 assets/images/controle-des-pots-1366.jpg 1366w,
				 assets/images/controle-des-pots-2732.jpg 2732w"
			 src="assets/images/controle-des-pots-1024.jpg"
			 alt="Contrôle des pots"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Contacts</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Prenez contact avec nous, venez visiter notre labo et déguster un sirop&nbsp;d’Agastache&nbsp;!</h2>
				</header>
			</div>
			<?php require_once 'components/person.php';
				$persons = array(
  				array(
    				"id" => "0",
						"slug" => "anne-lucie",
						"name" => "Anne-Lucie",
						"job" => "Responsable des RP",
						"phone" => "06 12 12 12 12",
						"mail" => "anne-lucie@pretapousser.fr"
					),
					array(
						"id" => "1",
						"slug" => "anne-lucie",
						"name" => "Anne-Lucie",
						"job" => "Responsable des RP",
						"phone" => "06 12 12 12 12",
						"mail" => "anne-lucie@pretapousser.fr"
					)
				);
			?>
			<ul class="newsroom__contacts people list">
				<li class="people__item row__col row__col--half-sm row__col--third-md row__col--quarter-lg">
					<?php displayPerson($persons[0]); ?>
				</li><!--
				--><li class="people__item row__col row__col--half-sm row__col--third-md row__col--quarter-lg">
				<?php displayPerson($persons[1]); ?>
			</li>
			</ul>
		</div>
	</section>
	<section class="section section--with-alt-background-color">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Plus d'informations</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Apprenez-en plus sur Prêt à&nbsp;Pousser</h2>
					<p class="text text--medium text--big-md text--centered">Découvrez toutes nos rubriques pour vous informer sur Prêt à Pousser, son histoire, son labo, sa technologie brevetée, sa nouvelle App mobile et toutes autres&nbsp;actualités.</p>
				</header>
			</div>
			<?php include 'components/news-list.php'; ?>
		</div>
	</section>
</div>

<?php include 'components/footer.php'; ?>
