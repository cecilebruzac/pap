(function () {

  $('.slider').slick({
    dots: true,
    arrows: false
  });

  $('.slider-with-arrows').slick({
    dots: true,
    arrows: true
  });

  $('#modular-slider').slick({
    dots: true,
    arrows: false,
    customPaging: function (slider, i) {
      const label = $(slider.$slides[i]).data('label');
      return '<button>' + label + '</button>';
    },
    appendDots: $('#modular-slider-nav'),
  });

  animateNav();
  animateAccordions();

  /*TODO: refactor in pure js*/
  $('a[href^="#"]').on('click', function (event) {
    const target = $(this.getAttribute('href'));
    if (target.length > 0) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top - $(window).height() / 4
      }, 600);
    }
  });
}());


function toogleMenu(element, menuId, isFullHeight) {
  element.classList.toggle('toggler--active');
  const menu = document.getElementById(menuId);

  if (menu !== null) {
    if (isFullHeight === false) {
      menu.style.height = menu.firstElementChild.offsetHeight + 'px';
    }

    menu.classList.toggle('menu--visible');

    if (isFullHeight === false) {
      window.setTimeout(function () {
        menu.removeAttribute('style');
      }, 200);
    }
  }
}

function removeElement(elementId) {
  const element = document.getElementById(elementId);
  if (element !== null) {
    element.parentNode.removeChild(element);
  }
}

function animateNav() {
  const navRowToHideOnScroll = document.getElementById('nav-row-to-hide-on-scroll');
  const brandToReduceOnScroll = document.getElementById('brand-to-reduce-on-scroll');

  if (navRowToHideOnScroll !== null && brandToReduceOnScroll !== null) {
    let navRowToHideOnScrollHeight = navRowToHideOnScroll.clientHeight;
    let brandToReduceOnScrollHeight = brandToReduceOnScroll.clientHeight;

    window.addEventListener('resize', function () {
      reset();
      navRowToHideOnScrollHeight = navRowToHideOnScroll.clientHeight;
      brandToReduceOnScrollHeight = brandToReduceOnScroll.clientHeight;
      update();
    }, true);

    window.addEventListener('scroll', function () {
      if (navRowToHideOnScrollHeight > 0) {
        update();
      } else {
        reset();
      }
    });

    function update() {
      let delta = navRowToHideOnScrollHeight - (window.scrollY || window.pageYOffset);
      navRowToHideOnScroll.style.overflow = 'hidden';

      if (delta >= 0) {
        navRowToHideOnScroll.style.height = delta + 'px';
        brandToReduceOnScroll.style.height = brandToReduceOnScrollHeight - (navRowToHideOnScrollHeight - delta) + 'px';
      } else {
        navRowToHideOnScroll.style.height = '0px';
        brandToReduceOnScroll.style.height = brandToReduceOnScrollHeight - navRowToHideOnScrollHeight + 'px';
      }
    }

    function reset() {
      navRowToHideOnScroll.removeAttribute('style');
      brandToReduceOnScroll.removeAttribute('style');
    }

  }
}

function animateAccordions() {
  let acc = document.getElementsByClassName('accordion__cta');
  if (acc.length > 0) {
    const types = ['click', 'keypress'];
    for (let i = 0; i < acc.length; i++) {
      for (let t = 0; t < types.length; t++) {
        acc[i].addEventListener(types[t], function () {
          this.classList.toggle('accordion__cta--active');
          let panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
          } else {
            panel.style.maxHeight = panel.scrollHeight + 'px';
          }
        });
      }
    }
  }
}

function showPhotoInScene(event, element, sceneId) {
  event.preventDefault();

  const scene = document.getElementById(sceneId);

  if (scene !== null) {
    const sceneImage = scene.firstElementChild;

    if (sceneImage !== null) {
      scene.style.backgroundImage = 'url(' + sceneImage.src + ')';
      sceneImage.classList.add('hide');

      window.setTimeout(function () {
        scene.firstElementChild.src = element.href;
        sceneImage.classList.remove('hide');
      }, 100);
    }
  }
}
