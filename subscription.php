<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  displayMainNav();
?>
<div class="subscription">
	<header class="subscription__intro">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<h1 class="subscription__title text text--bold text--centered text--big text--huge-md">Abonnement</h1>
				<p class="text text--big text--bigger-md text--centered">Recevez vos capsules à la fréquence de votre choix
					directement chez&nbsp;vous.</p>
			</div>
		</div>
	</header>
	<div class="subscription__capsules">
		<picture>
			<source media="(max-width: 23.4375em)"
							sizes="100vw"
							srcset="assets/images/capsules-sm-375.jpg 375w,
							assets/images/capsules-sm-750.jpg 750w">
			<source media="(max-width: 50em)"
							sizes="100vw"
							srcset="assets/images/capsules-md-800.jpg 800w,
							assets/images/capsules-md-1600.jpg 1600w">
			<source media="(min-width: 50em)"
							sizes="100vw"
							srcset="assets/images/capsules-lg-1366.jpg 1366w,
							assets/images/capsules-lg-2732.jpg 2732w">
			<img class="subscription__capsules-ill"
					 src="assets/images/capsules-lg-1366.jpg"
					 alt="Capsules"/>
		</picture>
	</div>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<h2 class="text text--big text--bigger-md text--bold text--centered">Simplifiez-vous la vie et laissez-vous&nbsp;surprendre&nbsp;!</h2>
					<p class="text text--medium text--big-md text--centered">L’abonnement Prêt à Pousser a été conçu pour vous
						simplifier la vie. Précisez-nous simplement le type de plantes que vous aimez, la fréquence de livraison et
						recevez des plantes tout à la&nbsp;maison.</p>
				</header>
				<p class="text text--medium text--big-md text--centered"><a class="button button--sale" href="#">Je m'abonne</a>
				</p>
			</div>
		</div>
	</section>

	<section class="section section--with-list section--with-alt-background-color">
		<div class="row row--centered">
			<ul class="section__list subscription__arguments">
				<li class="section__list-item row__col row__col--half-sm row__col--third-md">
					<h2 class="subscription__argument-title text text--big text--bold text--uppercase text--centered">Aucun
						engagement</h2>
					<p class="text text--medium text--centered">Vous pouvez arrêter votre abonnement à tout moment, en un&nbsp;clic.</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--third-md">
					<h2 class="subscription__argument-title text text--big text--bold text--uppercase text--centered">Aucun frais
						d'abonnement</h2>
					<p class="text text--medium text--centered">L’abonnement est gratuit. Vous ne payez que les plantes que vous
						recevez et bénéficiez d’une remise de 15 à 25&#x202f;% en fonction de la quantité&nbsp;choisie.</p>
				</li><!--
				--><li class="section__list-item row__col row__col--half-sm row__col--third-md">
					<h2 class="subscription__argument-title text text--big text--bold text--uppercase text--centered">Modification
						en 1 clic</h2>
					<p class="text text--medium text--centered">Avant chaque envoi, vous recevez un email qui vous offre la
						possibilité de changer vos&nbsp;plantes.</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="subscription__kitchen-bench section">
		<picture>
			<source media="(min-width: 50em)"
							sizes="100vw"
							srcset="assets/images/paillasse-800.jpg 800w,
							assets/images/paillasse-1024.jpg 1024w,
							assets/images/paillasse-1600.jpg 1600w,
							assets/images/paillasse-2048.jpg 2048w,
							assets/images/paillasse-1366.jpg 1366w,
							assets/images/paillasse-2732.jpg 2732w">
			<img class="subscription__kitchen-bench-ill"
					 sizes="100vw"
					 srcset="assets/images/paillasse-crop-375.jpg 375w,
					 assets/images/paillasse-crop-750.jpg 750w,
					 assets/images/paillasse-crop-800.jpg 800w,
					 assets/images/paillasse-crop-1600.jpg 1600w"
					 src="assets/images/paillasse-1024.jpg"
					 alt="Paillasse"/>
		</picture>
		<div class="row row--centered">
			<div class="row__col row__col--half-sm">
				<h2 class="text text--bold text--big text--huge-md">Vous ne serez plus jamais à court de&nbsp;plantes.</h2>
				<p class="text text--big text--bigger-md">Recevez le meilleur des plantes Prêt à Pousser tout au
					long de&nbsp;l’année.</p>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<h2 class="text text--big text--bigger-md text--bold text--centered">Découvrez de nouvelles variétés à chaque&nbsp;envoi.</h2>
				<p class="text text--medium text--big-md text--centered">Nous sélectionnons les capsules en prenant en compte
					vos goûts et envies. Un lot de surprises vous attend dans chaque&nbsp;colis&nbsp;!</p>
			</div>
		</div>
	</section>
	<div class="subscription__modulos">
		<img class="subscription__modulos-ill"
				 sizes="(min-width: 71.75em) 70.5em, 100vw"
				 srcset="assets/images/modulos-375.jpg 375w,
				 assets/images/modulos-750.jpg 750w,
				 assets/images/modulos-1024.jpg 1024w,
				 assets/images/modulos-2048.jpg 2048w,
				 assets/images/modulos-1128.jpg 1128w,
				 assets/images/modulos-2256.jpg 2256w"
				 src="assets/images/modulos-1024.jpg"
				 alt="Modulo"/>
	</div>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<h2 class="text text--big text--bigger-md text--bold text--centered">Créez votre abonnement en 2 minutes&nbsp;chrono.</h2>
					<p class="text text--medium text--big-md text--centered">Créez votre abonnement en quelques instants,
						facilement, en 3 étapes. Vous pouvez le modifier à tout moment si vos envies&nbsp;évoluent.</p>
				</header>
				<p class="text text--medium text--big-md text--centered"><a class="button button--sale" href="#">Je m'abonne</a>
				</p>
			</div>
		</div>
	</section>
	<?php
	    require_once 'components/content-sections/faq.php';
	    $questions = array(
	      array(
	        "question" => "Y a-t-il des coûts si je décide de mettre en pause mon abonnement pendant une certaine&nbsp;durée&nbsp;?",
					"answer" => "Aucun coût caché&nbsp;! Nous avons conçu l’offre d’abonnement pour vous simplifier la vie. Si vous partez
					en vacances ou si vous avez trop de capsules chez vous, mettez simplement votre abonnement en pause et réactivez-le
					dès que souhaité. Toute cette période d’interruption ne vous sera pas facturée. Seule les commandes reçues sont à&nbsp;payer."
				),
				array(
					"question" => "Est-il possible de modifier les plantes sélectionnées à l’intérieur&nbsp;abonnement&nbsp;?",
					"answer" => "Oui. À tout moment, vous pouvez changer la variété et quantité de plantes à recevoir. Nous vous envoyons
					un email récapitulatif une semaine avant l’envoi de votre abonnement. Ce qui vous laisse le temps d’adapter votre
					livraison selon vos envies du&nbsp;moment."
				),
				array(
					"question" => "À quelle moment suis-je&nbsp;prélevé&nbsp;?",
					"answer" => "Le moyen de paiement que vous avez renseigné (CB, Paypal, etc) sera débité au moment où vous recevrez
					votre&nbsp;livraison."
				),
				array(
					"question" => "Puis-je arrêter mon abonnement n’importe&nbsp;quand&nbsp;?",
					"answer" => "Oui, depuis la <a class=\"text__link\" href=\"\">page de gestion de votre abonnement</a> de plantes, vous
					pouvez à tout moment résilier votre abonnement. La prise à effet est immédiate et sans coût&nbsp;caché."
				),
				array(
					"question" => "Suis-je livré directement dans ma boite aux&nbsp;lettres&nbsp;?",
					"answer" => "Nous avons conçu nos capsules de sorte à ce que leur épaisseur ne dépasse pas 2&nbsp;cm. Ainsi, le colis
					peut rentrer directement dans votre boite aux lettres, sans aller au bureau de&nbsp;Poste&nbsp;!"
				)
			);

	displayFaq($questions);
	?>
</div>

<?php include 'components/footer.php'; ?>
