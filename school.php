<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>
<div class="labo">
	<nav class="school__nav secondary-nav">
		<div class="row row--centered">
			<div class="secondary-nav__wrapper">
				<div class="secondary-nav__col">
					<p class="text text--medium text--bigger-md text--bold">Prêt à Pousser</p>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
					<button class="secondary-nav__toggler toggler"
									onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
									aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
										 role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
					</button>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__show-md">
					<div class="secondary-nav__menu menu">
						<ul class="menu__list">
							<li class="menu__item">
								<a class="menu__link"
									 href="/team.php">
									<span class="text text--big">Qui sommes-nous</span>
								</a>
							</li><!--
							--><li class="menu__item">
								<a class="menu__link" href="/recruitment.php">
									<span class="text text--big">Recrutement</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="/labo.php">
									<span class="text text--big">Notre labo R&D</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="#">
									<span class="text text--big">Notre histoire</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link menu__link--active menu__link--active-border" href="/school.php">
									<span class="text text--big">1 Kit 1 École</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="collapsable-secondary-menu"
					 class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
				<ul class="menu__list">
					<li class="menu__item">
						<a class="menu__link"
							 href="/team.php">
							<span class="text text--big">Qui sommes-nous</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/recruitment.php">
							<span class="text text--big">Recrutement</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/labo.php">
							<span class="text text--big">Notre labo R&D</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="#">
							<span class="text text--big">Notre histoire</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link menu__link--active" href="/school.php">
							<span class="text text--big">1 Kit 1 École</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<header class="school__intro">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<h1 class="school__title text text--bold text--centered text--big text--huge-md">Un Kit Une École</h1>
				<p class="text text--big text--bigger-md text--centered">Chez Prêt à Pousser, nous avons mis en place un
					programme participatif pour faire découvrir le monde végétal aux&nbsp;écoliers.</p>
			</div>
		</div>
	</header>
	<div class="row row--centered">
		<img class="school__ill"
				 sizes="(min-width: 71.75em) 70.5em, calc(100vw - 1.25em)"
				 srcset="assets/images/classe-355.jpg 355w,
				 assets/images/classe-710.jpg 710w,0
				 assets/images/classe-1004.jpg 1004w,
				 assets/images/classe-2008.jpg 2008w,
				 assets/images/classe-1128.jpg 1128w,
				 assets/images/classe-2256.jpg 2256w"
				 src="assets/images/classe-1128.jpg"
				 alt="École"/>
	</div>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Le chiffre qui
						fait du bien</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Plus de 80 000 écoliers ont déjà profité
						du programe 1 kit 1&nbsp;École</h2>
				</header>
				<p class="text text--medium text--centered">
					<img class="school__ill"
							 sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
							 srcset="assets/images/enfants-avec-pleurotes-355.jpg 355w,
							 assets/images/enfants-avec-pleurotes-710.jpg 710w,
							 assets/images/enfants-avec-pleurotes-677.jpg 677w,
							 assets/images/enfants-avec-pleurotes-1354.jpg 1354w"
							 src="assets/images/enfants-avec-pleurotes-677.jpg"
							 alt="Enfants avec le kit de pleurotes"/>
				</p>
				<p class="text text--medium text--big-md text--centered">
					«&nbsp;Ça en met du temps à pousser les plantes en&nbsp;vrai.&nbsp;»<br>
					—Arthur, 6&nbsp;ans
				</p>
			</div>
		</div>
	</section>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Participez</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Faites bénéficier gratuitement 1 Kit à
						l’école de votre&nbsp;choix.</h2>
				</header>
				<ol class="school__ordonated-list">
					<li class="text text--medium text--big-md">
						Prenez une photo de vous et votre&nbsp;produit
					</li>
					<li class="text text--medium text--big-md">
						Partagez-la sur notre page facebook avec l’hashtag «&nbsp;#1kit1école&nbsp;»
					</li>
					<li class="text text--medium text--big-md">
						Remplissez le formulaire&nbsp;ci-dessous.
					</li>
				</ol>
			</div>
		</div>
	</section>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<h2 class="text text--big text--bigger-md text--bold text--centered">Quel produit avez-vous acheté et partagé
						sur&nbsp;Facebook&nbsp;?</h2>
				</header>
			</div>
		</div>
	</section>
	<hr>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Accompagnement
						pédagogique</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Un programme en conformité avec les
						programmes&nbsp;scolaires</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Faire pousser des champignons ou des plantes en classe s’inscrit dans la démarche de l’éducation scolaire. En
					plus d’envoyer un kit, nous offrons aussi un dossier à destination des enseignants afin de les accompagner
					dans cette démarche de sensibilisation. Nous souhaitons rendre le programme 1 Kit 1 École le plus ludique
					possible pour les&nbsp;enfants.
				</p>
			</div>
		</div>
	</section>
	<div class="row row--centered">
		<img class="school__ill"
				 sizes="(min-width: 71.75em) 70.5em, calc(100vw - 1.25em)"
				 srcset="assets/images/rempotage-355.jpg 355w,
				 assets/images/rempotage-710.jpg 710w,
				 assets/images/rempotage-1004.jpg 1004w,
				 assets/images/rempotage-2008.jpg 2008w,
				 assets/images/rempotage-1128.jpg 1128w,
				 assets/images/rempotage-2256.jpg 2256w"
				 src="assets/images/rempotage-1128.jpg"
				 alt="Rempotage"/>
	</div>
	<section class="section school__gallery">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Notre mission</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Nous sensibilisons les enfants au vivant
						et à la&nbsp;nature</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Nous souhaitons transmettre aux enfants les valeurs qui nous sont chères : la nature, le développement
					durable, le bien manger, le partage et la patience. Nous pensons qu’il est important de participer à la
					sensibilisation des enfants à l’écologie, il en va du monde de demain. Le respect de la biosphère passe par
					des gestes simples du quotidien. Les kits pédagogiques que nous offrons permettent aux enfants d’approcher et
					faire grandir des plantes et des champignons de leurs mains.
				</p>
				<div class="slider-with-arrows">
					<div>
						<img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
								 srcset="assets/images/ecolier-355.jpg 355w,
								 assets/images/ecolier-710.jpg 710w,
								 assets/images/ecolier-677.jpg 677w,
								 assets/images/ecolier-1354.jpg 1354w"
								 src="assets/images/ecolier-677.jpg"
								 width="677"
								 alt="Écolier"/>
					</div>
					<div>
						<img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
								 srcset="assets/images/ecolier-355.jpg 355w,
								 assets/images/ecolier-710.jpg 710w,
								 assets/images/ecolier-677.jpg 677w,
								 assets/images/ecolier-1354.jpg 1354w"
								 src="assets/images/ecolier-677.jpg"
								 width="677"
								 alt="Écolier"/>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<hr>
<?php include 'components/footer.php'; ?>
