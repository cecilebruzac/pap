<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  displayMainNav();
?>

<div class="mobile-app">
	<section class="mobile-app__intro section">
		<div class="row row--centered">
			<div class="row__col row__col--two-fifths-md row__col--with-right-gutter-md">
				<h1 class="text text--bold text--big text--huge-md">L’app mobile<br>Prêt à&nbsp;Pousser</h1>
				<p class="text text--big text--bigger-md">Nous avons conçu une app mobile iOS et Android pour vous accompagner
					dans la pousse de vos&nbsp;plantes.</p>
				<p class="mobile-app__badge-wrapper text text--medium">
					<a href=""
						 title="Télécharger dans l'App Store">
						<img class="mobile-app__badge"
								 src="assets/images/app-store-badge.svg"
								 width="200"
								 alt="App Store"/>
					</a>
				</p>
				<p class="mobile-app__badge-wrapper text text--medium mobile-app__badge">
					<a href=""
						 title="Télécharger sur Google Play">
						<img class="mobile-app__badge"
								 src="assets/images/google-play-badge.svg"
								 width="200"
								 alt="Google Play"/>
					</a>
				</p>
			</div><!--
			--><div class="row__col row__col--three-fifths-md row__col--with-left-gutter-md">
				<p class="mobile-app__device-ill-wrapper text text--medium text--centered">
					<img class="mobile-app__intro-ill mobile-app__device-ill"
							 src="assets/images/mobile-app.png"
							 srcset="assets/images/mobile-app@2x.png 2x"
							 width="409"
							 alt="app mobile"/>
				</p>
			</div>
		</div>
	</section>
	<section class="section section--with-alt-background-color">
		<div class="row row--centered">
			<div class="mobile-app__device-col row__col row__col--two-fifths-md row__col--with-right-gutter-md">
				<p class="mobile-app__device-ill-wrapper text text--medium text--centered">
					<img class="mobile-app__device-ill"
							 src="assets/images/mobile-app-conseils.png"
							 srcset="assets/images/mobile-app-conseils@2x.png 2x"
							 width="351"
							 alt="Conseils"/>
				</p>
			</div><!--
      --><div class="mobile-app__device-col row__col row__col--three-fifths-md row__col--with-left-gutter-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light">Conseils de pousse</p>
					<h2 class="text text--big text--bigger-md text--bold">Recevez les astuces de notre ami&nbsp;jardinier</h2>
				</header>
				<p class="text text--medium text--big-md">Chaque plante est unique. Chaque pousse l’est tout autant. Pour vous
					accompagner, nous avons conçu une App mobile simple et efficace. À chaque étape de pousse, notre jardinier
					peut vous conseiller et vous aider à avoir la main plus verte de jour en&nbsp;jour.</p>
			</div>
		</div>
	</section>
	<section class="section">
		<div class="row row--centered">
			<div class="mobile-app__device-col row__col row__col--three-fifths-md row__col--with-right-gutter-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light">Assistance</p>
					<h2 class="text text--big text--bigger-md text--bold">Une photo vaut milles&nbsp;mots</h2>
				</header>
				<p class="text text--medium text--big-md">Vous avez une question&nbsp;? Vous vous demandez si votre plante
					pousse normalement&nbsp;? L’app Prêt à Pousser permet d’envoyer une photo très simplement. Dès réception, nous
					reviendrons vers vous aussi vite que&nbsp;possible.</p>
			</div><!--
			--><div class="mobile-app__device-col row__col row__col--two-fifths-md row__col--with-left-gutter-md">
				<p class="mobile-app__device-ill-wrapper text text--medium text--centered">
					<img class="mobile-app__device-ill"
							 src="assets/images/mobile-app-assistance.png"
							 srcset="assets/images/mobile-app-assistance@2x.png 2x"
							 width="410"
							 alt="Assistance"/>
				</p>
			</div>
		</div>
	</section>
</div>
<div class="gardens">
	<section class="gardens__introduction section">
		<div class="row row--centered show-md">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<p class="text text--big text--bigger-md text--uppercase text--light text--centered">La gamme de potagers&nbsp;d'intérieur</p>
				<h2 class="text text--big text--bigger-md text--bold text--centered">L’App Prêt à Pousser c’est bien, mais avec
					un potager, c’est encore &nbsp;mieux&nbsp;!</h2>
			</div>
		</div>
		<picture class="gardens__illustration">
			<source media="(min-width: 50em)"
							sizes="100vw"
							srcset="assets/images/potagers-800.jpg 800w,
							assets/images/potagers-1024.jpg 1024w,
							assets/images/potagers-1600.jpg 1600w,
							assets/images/potagers-2048.jpg 2048w,
							assets/images/potagers-1366.jpg 1366w,
							assets/images/potagers-2732.jpg 2732w">
			<img sizes="100vw"
					 srcset="assets/images/potagers-crop-375.jpg 375w,
					 assets/images/potagers-crop-750.jpg 750w,
					 assets/images/potagers-crop-800.jpg 800w,
					 assets/images/potagers-crop-1600.jpg 1600w"
					 src="assets/images/potagers-1024.jpg"
					 alt="Les potagers d'intérieur"/>
		</picture>
		<div class="row row--centered">
			<ul class="gardens__list">
				<li class="gardens__item row__col row__col--half-sm">
					<article class="gardens__garden garden">
						<a href="/modulo.php">
							<h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">MODULO</h2>
							<h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre potager d’intérieur&nbsp;évolutif</h3>
						</a>
						<p class="garden__price text text--big text--bigger-md">149,95 €</p>
						<div class="garden__cta text--big-md">
							<button class="button button--sale">Acheter</button>
						</div>
						<p class="garden__see-more text text--medium">
							<a class="text__link" href="/modulo.php">Découvrir Modulo</a>
						</p>
					</article>
				</li><!--
        --><li class="gardens__item row__col row__col--half-sm">
					<article class="gardens__garden garden">
						<a href="/lilo.php">
							<h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">LILO</h2>
							<h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre jardin à portée
								de&nbsp;main</h3>
						</a>
						<p class="garden__price text text--big text--bigger-md">99,95 €</p>
						<div class="garden__cta text--big-md">
							<button class="button button--sale">Acheter</button>
						</div>
						<p class="garden__see-more text text--medium">
							<a class="text__link" href="/lilo.php">Découvrir Lilo</a>
						</p>
					</article>
				</li>
			</ul>
		</div>
	</section>
</div>

<?php include 'components/footer.php'; ?>