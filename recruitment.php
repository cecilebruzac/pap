<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>
<div class="labo">
	<nav class="recruitment__nav secondary-nav">
		<div class="row row--centered">
			<div class="secondary-nav__wrapper">
				<div class="secondary-nav__col">
					<p class="text text--medium text--bigger-md text--bold">Prêt à Pousser</p>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
					<button class="secondary-nav__toggler toggler"
									onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
									aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
										 role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
					</button>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__show-md">
					<div class="secondary-nav__menu menu">
						<ul class="menu__list">
							<li class="menu__item">
								<a class="menu__link"
									 href="/team.php">
									<span class="text text--big">Qui sommes-nous</span>
								</a>
							</li><!--
							--><li class="menu__item">
								<a class="menu__link menu__link--active menu__link--active-border" href="/recruitment.php">
									<span class="text text--big">Recrutement</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="/labo.php">
									<span class="text text--big">Notre labo R&D</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="#">
									<span class="text text--big">Notre histoire</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="/school.php">
									<span class="text text--big">1 Kit 1 École</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="collapsable-secondary-menu"
					 class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
				<ul class="menu__list">
					<li class="menu__item">
						<a class="menu__link"
							 href="/team.php">
							<span class="text text--big">Qui sommes-nous</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link menu__link--active" href="/recruitment.php">
							<span class="text text--big">Recrutement</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/labo.php">
							<span class="text text--big">Notre labo R&D</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="#">
							<span class="text text--big">Notre histoire</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/school.php">
							<span class="text text--big">1 Kit 1 École</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<header class="recruitment__intro">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<h1 class="recruitment__title text text--bold text--centered text--big text--huge-md">Rejoignez l’équipe Prêt à&nbsp;Pousser</h1>
				<p class="text text--big text--bigger-md text--centered">Nous cherchons des personnes passionnées,
					ambitieuses et&nbsp;dégourdies.</p>
			</div>
		</div>
	</header>
	<img class="recruitment__ill"
			 sizes="100vw"
			 srcset="assets/images/equipe-375.jpg 375w,
				 assets/images/equipe-750.jpg 750w,
				 assets/images/equipe-1024.jpg 1024w,
				 assets/images/equipe-2048.jpg 2048w,
				 assets/images/equipe-1366.jpg 1366w,
				 assets/images/equipe-2732.jpg 2732w"
			 src="assets/images/equipe-1024.jpg"
			 alt="Équipe Prêt à Pousser"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Profils
						recherchés</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Vous souhaitez rejoindre notre&nbsp;équipe&nbsp;?</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Vous êtes gourmand(e)&nbsp;? Vous êtes curieux(se)&nbsp;? Vous voulez travailler avec une équipe jeune et
					travailleuse, dans une bonne ambiance&nbsp;? Vous êtes passionné(e) par ce que vous faîtes et vous vous donnez
					à 100&#x202f;%&nbsp;? Si vous avez répondu "OUI" à toutes ces questions, nous sommes intéressés&nbsp;! Envoyez
					nous votre candidature sur <a class="text__link" href="mailto:&#099;&#097;&#110;&#100;&#105;&#100;&#097;&#116;&#117;&#114;&#101;&#064;&#112;&#114;&#101;&#116;&#097;&#112;&#111;&#117;&#115;&#115;&#101;&#114;&#046;&#102;&#114;">&#099;&#097;&#110;&#100;&#105;&#100;&#097;&#116;&#117;&#114;&#101;&#064;&#112;&#114;&#101;&#116;&#097;&#112;&#111;&#117;&#115;&#115;&#101;&#114;&#046;&#102;&#114;</a>
					pour des stages, CDD, et CDI. Malheureusement, nous ne prenons pas d'alternants pour&nbsp;l'instant.
				</p>
			</div>
			<ul class="recruitment__ads">
				<li class="recruitment__ad">
					<p class="text text--medium text--big-md">Chargé(e) de communication</p>
					<p class="text text--medium">Paris <span class="text--light-grey">●</span> Stage</p>
				</li>
				<li class="recruitment__ad">
					<p class="text text--medium text--big-md">Chargé(e) de web marketting</p>
					<p class="text text--medium">Paris <span class="text--light-grey">●</span> Stage</p>
				</li>
				<li class="recruitment__ad">
					<p class="text text--medium text--big-md">Web developer</p>
					<p class="text text--medium">Paris <span class="text--light-grey">●</span> Stage</p>
				</li>
				<li class="recruitment__ad">
					<p class="text text--medium text--big-md">Web designer</p>
					<p class="text text--medium">Paris <span class="text--light-grey">●</span> Stage</p>
				</li>
			</ul>
		</div>
	</section>
	<hr class="show-md">
	<section class="section recruitment__gallery show-md">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered">Des photos
						de&nbsp;nous</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Découvrez la vie chez Prêt
						à&nbsp;Pousser</h2>
				</header>
				<div class="slider-with-arrows">
					<div>
						<img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
								 srcset="assets/images/laboratoire-355.jpg 355w,
								 assets/images/laboratoire-710.jpg 710w,
								 assets/images/laboratoire-677.jpg 677w,
								 assets/images/laboratoire-1354.jpg 1354w"
								 src="assets/images/laboratoire-677.jpg"
								 width="677"
								 alt="Laboratoire"/>
					</div>
					<div>
						<img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
								 srcset="assets/images/laboratoire-355.jpg 355w,
								 assets/images/laboratoire-710.jpg 710w,
								 assets/images/laboratoire-677.jpg 677w,
								 assets/images/laboratoire-1354.jpg 1354w"
								 src="assets/images/laboratoire-677.jpg"
								 width="677"
								 alt="Laboratoire"/>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<hr>
<?php include 'components/footer.php'; ?>
