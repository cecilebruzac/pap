<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  displayMainNav();
?>
<div class="section row row--centered">
	<article class="product row__col row__col--quarter">
		<div class="checkbox checkbox--light">
			<input class="checkbox__input" type="checkbox" id="basilic">
			<label class="checkbox__label product__thumbnail" for="basilic">
				<div class="product__image-wrapper product__image-wrapper--square" style="background-color: #65C1C3">
					<img class="product__image product__image--seed"
							 src="assets/images/plants/basilic-grand-vert.png"
							 alt="Basilic grand&nbsp;vert">
				</div>
			</label>
		</div>
		<div class="product__content">
			<header class="product__header">
				<p class="product__category text text--light show-md">Plante</p>
				<div class="product__designation-and-price">
					<div class="product__designation">
						<h3 class="product__name text text--medium text--big-md text--bold">Basilic grand&nbsp;vert</h3>
					</div>
					<p class="product__price text text--big">5,95&#x202f;€</p>
				</div>
			</header>
			<p class="product__intro text text--medium show-md">Parce que vos tomates le méritent.</p>
		</div>
	</article>
	<p class="text text--medium">&nbsp;</p>
	<article class="product row__col row__col--quarter">
		<div class="checkbox checkbox--light checkbox--out">
			<input class="checkbox__input" type="checkbox" id="sauge">
			<label class="checkbox__label product__thumbnail" for="sauge">
				<div class="product__image-wrapper product__image-wrapper--square" style="background-color: #F19F67">
					<img class="product__image product__image--seed" src="assets/images/plants/sauge.png"
							 srcset="assets/images/plants/sauge@2x.png 2x" alt="Sauge"></div>
			</label>
		</div>
		<div class="product__content">
			<header class="product__header">
				<p class="product__category text text--light show-md">Plante</p>
				<div class="product__designation-and-price">
					<div class="product__designation">
						<h3 class="product__name text text--medium text--big-md text--bold">Sauge</h3>
					</div>
					<p class="product__price text text--big">5,95&#x202f;€</p>
				</div>
			</header>
			<p class="product__intro text text--medium show-md">Parce que vos tomates le méritent.</p>
		</div>
	</article>
	<p class="text text--medium">&nbsp;</p>
	<div class="checkbox checkbox--out">
		<input class="checkbox__input" type="checkbox" id="legumes">
		<label class="checkbox__label checkbox__label--with-border" for="legumes">
			<span class="text text--medium">Légumes</span>
		</label>
	</div>
	<p class="text text--medium">&nbsp;</p>
	<div class="checkbox">
		<input class="checkbox__input" type="checkbox" id="fleurs">
		<label class="checkbox__label checkbox__label--with-border" for="fleurs">
			<span class="text text--medium">Fleurs</span>
		</label>
	</div>
</div>

<hr>

<?php include 'components/footer.php'; ?>
