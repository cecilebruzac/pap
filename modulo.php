<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<div class="modulo">
  <nav class="modulo__nav secondary-nav">
    <div class="row row--centered">
      <div class="secondary-nav__wrapper">
        <div class="secondary-nav__col">
          <div class="secondary-nav__title-wrapper">
            <p class="text text--medium text--bigger-md text--bold">Modulo</p>
          </div><!--
          --><div class="secondary-nav__menu menu secondary-nav__show-md">
            <ul class="menu__list">
              <li class="menu__item">
                <a class="menu__link"
                   href="#video">
                  <span class="text text--big">Vidéo</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#garantie-pousse">
                  <span class="text text--big">Garantie Pousse</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link"
                   href="#plantes">
                  <span class="text text--big">Plantes</span>
                </a>
              </li>
            </ul>
        </div>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
          <button class="secondary-nav__toggler toggler"
                  onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
                  aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
                     role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
          </button>
        </div><!--
        --><div class="secondary-nav__col">
          <form class="add-in-cart-form">
            <input type="hidden"
                   name="product-id" value="1">
            <button class="add-in-cart-form__button button button--sale"
                    type="submit">Acheter
            </button>
          </form>
        </div>
      </div>
      <div id="collapsable-secondary-menu"
           class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link"
               href="#video">
              <span class="text text--big">Vidéo</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#garantie-pousse">
              <span class="text text--big">Garantie Pousse</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="#plantes">
              <span class="text text--big">Plantes</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section class="modulo__introduction">
    <div class="modulo__notification-wrapper row row--centered show-md">
      <div class="notification notification--with-shadow"
           id="gift-with-modulo">
        <p class="text text--medium text--bold">🎁 4 capsules incluses avec votre Modulo</p>
        <button class="notification__close-button"
                onclick="removeElement('gift-with-modulo')"
                aria-label="Effacer cette notification">
          <svg viewBox="0 0 15 15" width="15" height="15"
               role="img" aria-hidden="true">
            <use xlink:href="assets/images/defs.svg#close"></use>
          </svg>
        </button>
      </div>
    </div>
    <picture class="show-md">
      <source media="(min-width: 50em)"
              sizes="100vw"
              srcset="assets/images/modulo-fullscreen-800.png 800w,
							assets/images/modulo-fullscreen-1024.png 1024w,
							assets/images/modulo-fullscreen-1600.png 1600w,
							assets/images/modulo-fullscreen-2048.png 2048w,
							assets/images/modulo-fullscreen-1366.png 1366w,
							assets/images/modulo-fullscreen-2732.png 2732w">
      <img class="modulo__illustration"
           src="assets/images/modulo-fullscreen-1024.png"
           alt="Modulo"/>
    </picture>
    <div class="row row--centered">
      <article class="row__col row__col--third-md">
        <div class="modulo__infos">
          <p class="modulo__new text text--big text--light text--uppercase show-md">Nouveau</p>
          <h1 class="text text--bigger text--huge-md text--bold">Modulo</h1>
          <h2 class="text text--big text--bigger-md">votre potager d’intérieur&nbsp;évolutif</h2>
          <div class="modulo__price-wrapper">
            <p class="modulo__price text text--big text--bigger-md hide-md">149,95&#x202f;€</p>
          </div>
          <div class="modulo__gallery slider hide-md">
            <div>
              <picture>
                <source media="(max-width: 50em)"
                        sizes="calc(100vw - 1.25em)"
                        srcset="assets/images/modulo-355.jpg 355w,
                        assets/images/modulo-710.jpg 710w,
                        assets/images/modulo-780.jpg 780w,
                        assets/images/modulo-1560.jpg 1560w">
                <img src="assets/images/modulo-710.jpg"
                     alt="Modulo"/>
              </picture>
            </div>
            <div>
              <picture>
                <source media="(max-width: 50em)"
                        sizes="calc(100vw - 1.25em)"
                        srcset="assets/images/potager-pour-tous-355.jpg 355w,
                        assets/images/potager-pour-tous-710.jpg 710w,
                        assets/images/potager-pour-tous-780.jpg 780w,
                        assets/images/potager-pour-tous-1560.jpg 1560w">
                <img src="assets/images/potager-pour-tous-355.jpg"
                     alt="Lilo et l'enfant"/>
              </picture>
            </div>
          </div>
          <p class="modulo__mention text text--medium hide-md">Faites pousser vos légumes, salades et aromates dans
            votre cuisine, sans limite, pour toute votre&nbsp;famille.</p>
          <p class="modulo__price text text--big-sm text--bigger-lg show-md">149,95&#x202f;€</p>
          <div class="modulo__cta text--big">
            <button class="button button--sale">Acheter</button>
          </div>
          <p class="modulo__mention text text--medium show-md">Faites pousser vos légumes, salades et aromates dans
            votre cuisine, sans limite, pour toute votre&nbsp;famille.</p>
        </div>
      </article>
    </div>
    <div class="modulo__insurances">
      <div class="row row--centered">
        <ul class="modulo__insurances-list list list--style-icon">
          <li class="modulo__insurance list__item list__item--style-icon row__col row__col--third-md">
            <svg class="modulo__insurance-icon list__icon"
                 viewBox="0 0 38 36" width="38" height="36"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#stock"></use>
            </svg>
            <span class="text text--medium">
              <strong class="text--bold">En stock</strong><br>Recevez Modulo à partir du Lundi <strong class="text--bold">18&nbsp;Juin</strong>
            </span>
          </li><!--
          --><li class="modulo__insurance list__item list__item--style-icon row__col row__col--third-md">
            <svg class="modulo__insurance-icon list__icon"
                 viewBox="0 0 35 36" width="35" height="36"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#gardener"></use>
            </svg>
            <span class="text text--medium">
              <strong class="text--bold">2431 personnes</strong><br>utilisent un potager d’intérieur Prêt à Pousser en ce&nbsp;moment.
            </span>
          </li><!--
          --><li class="modulo__insurance list__item list__item--style-icon row__col row__col--third-md">
            <svg class="modulo__insurance-icon list__icon"
                 viewBox="0 0 35 35" width="35" height="35"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#growth"></use>
            </svg>
            <span class="text text--medium">
              <strong class="text--bold">Garantie Pousse</strong><br>Ça va pousser, c’est satisfait ou&nbsp;échangé,
              <a class="text__link" href="">en&nbsp;savoir&nbsp;plus</a>.
            </span>
          </li>
        </ul>
      </div>
    </div>
  </section>
  <div class="modulo__awards show-md">
    <div class="row row--centered">
      <img class="modulo__awards-logo"
           src="assets/images/selection-sial-innovation-paris-2018.png"
           srcset="assets/images/selection-sial-innovation-paris-2018@2x.png 2x"
           alt="Sélection du Grand Prix SIAL Innovation Paris 2018"
           height="45"/>
      <p class="modulo__awards-label text">Modulo fait partie de la sélection du Grand Prix SIAL Innovation Paris&nbsp;2018</p>
    </div>
  </div>
  <hr class="hide-md">
  <section class="section modulo__video"
           id="video">
    <div class="row row--centered">
      <header class="modulo__video-header section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Vidéo</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered">Modulo en 48&nbsp;secondes</h2>
        <p class="text text--medium text--big-md text--centered">Juré, pas une de&nbsp;plus</p>
      </header>
      <div class="row__col row__col--three-fifths-md row__col--centered-md">
        <p class="video-iframe">
          <iframe title="Lilo"
                  width="560"
                  height="315"
                  src="https://www.youtube.com/embed/B9BB3yOzvIo"
                  frameBorder="0"
                  allow="autoplay; encrypted-media"
                  allowFullScreen></iframe>
        </p>
      </div>
    </div>
  </section>

  <div id="garantie-pousse">
    <?php include 'components/content-sections/it-will-grow.php'; ?>
  </div>

  <?php include 'components/content-sections/it-s-natural.php'; ?>

  <section class="section modulo__modular modular">
    <div class="row row--centered">
      <header class="section__header">
        <div class="row__col row__col--centered row__col--three-fifths-md">
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Modulable</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Modulo s’adapte à votre&nbsp;intérieur</h2>
        </div>
        <nav id="modular-slider-nav"
             class="modulo__modular-nav"></nav>
      </header>
      <div id="modular-slider">
        <div data-label="sur votre mur">
          <img sizes="(min-width: 71.75em) 70.5em, calc(100vw - 1.25em)"
               srcset="assets/images/mo-du-lo-355.jpg 355w,
               assets/images/mo-du-lo-710.jpg 710w,
               assets/images/mo-du-lo-1004.jpg 1004w,
               assets/images/mo-du-lo-2008.jpg 2008w,
               assets/images/mo-du-lo-1128.jpg 1128w,
               assets/images/mo-du-lo-2256.jpg 2256w"
               src="assets/images/mo-du-lo-1128.jpg"
               alt="Modulo"/>
        </div>
        <div data-label="au sol">
          <img sizes="(min-width: 71.75em) 70.5em, calc(100vw - 1.25em)"
               srcset="assets/images/mo-du-lo-355.jpg 355w,
               assets/images/mo-du-lo-710.jpg 710w,
               assets/images/mo-du-lo-1004.jpg 1004w,
               assets/images/mo-du-lo-2008.jpg 2008w,
               assets/images/mo-du-lo-1128.jpg 1128w,
               assets/images/mo-du-lo-2256.jpg 2256w"
               src="assets/images/mo-du-lo-1128.jpg"
               alt="Modulo"/>
        </div>
        <div data-label="l’un sur l'autre">
          <img sizes="(min-width: 71.75em) 70.5em, calc(100vw - 1.25em)"
               srcset="assets/images/mo-du-lo-355.jpg 355w,
               assets/images/mo-du-lo-710.jpg 710w,
               assets/images/mo-du-lo-1004.jpg 1004w,
               assets/images/mo-du-lo-2008.jpg 2008w,
               assets/images/mo-du-lo-1128.jpg 1128w,
               assets/images/mo-du-lo-2256.jpg 2256w"
               src="assets/images/mo-du-lo-1128.jpg"
               alt="Modulo"/>
        </div>
      </div>
    </div>
  </section>

  <?php

    require_once 'components/content-sections/faq.php';

    $questions = array(
      array(
        "question" => "Pourquoi Modulo&nbsp;?",
        "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
        Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
        traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
        percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
        potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
        rector ausus miserabiles casus levare multorum."
      ),
      array(
        "question" => "Comment Modulo&nbsp;?",
        "answer" => "Per hoc minui studium suum existimans Paulus, ut erat in conplicandis negotiis artifex dirus, unde ei
        Catenae inditum est cognomentum, vicarium ipsum eos quibus praeerat adhuc defensantem ad sortem periculorum communium
        traxit. et instabat ut eum quoque cum tribunis et aliis pluribus ad comitatum imperatoris vinctum perduceret: quo
        percitus ille exitio urgente abrupto ferro eundem adoritur Paulum. et quia languente dextera, letaliter ferire non
        potuit, iam districtum mucronem in proprium latus inpegit. hocque deformi genere mortis excessit e vita iustissimus
        rector ausus miserabiles casus levare multorum."
      )
  );

  displayFaq($questions);

  ?>

  <section class="section modulo__light show-md">
    <div class="row row--centered">
      <img class="modulo__light-ill"
           sizes="(min-width: 71.75em) 70.5em, calc(100vw - 1.25em)"
           srcset="assets/images/modulo-light-355.jpg 355w,
               assets/images/modulo-light-710.jpg 710w,
               assets/images/modulo-light-1004.jpg 1004w,
               assets/images/modulo-light-2008.jpg 2008w,
               assets/images/modulo-light-1128.jpg 1128w,
               assets/images/modulo-light-2256.jpg 2256w"
           src="assets/images/modulo-light-1128.jpg"
           alt="Lampe de Modulo"/>
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Lumière performante et&nbsp;adaptable</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">La lumière du soleil dans votre potager&nbsp;d’intérieur.</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">Le luminaire s’allume automatiquement le matin et s ‘éteint le soir.
          Équipé de dizaines de LEDs basse consommation, il sait reproduire la lumière du soleil pour répondre aux
          besoins de vos plantes. En savoir plus sur <a href="/technology.php" class="text__link">notre technologie</a>.</p>
      </div>
    </div>
  </section>

  <hr class="hide-md">

  <?php include 'components/content-sections/simple-and-autonomous-device.php'; ?>

  <hr>

  <section class="section modulo__plants"
           id="plantes">
    <div class="row row--centered">
      <header class="section__header">
        <p class="text text--big text--bigger-md text--uppercase text--light text--centered">Les plantes</p>
        <h2 class="text text--big text--bigger-md text--bold text--centered">Plus de 40 variétés d’aromates, mini
          légumes et fleurs à cultiver.</h2>
      </header>
      <?php include 'components/plants-list.php'; ?>
    </div>
  </section>
</div>

<hr>

<?php include 'components/footer.php'; ?>