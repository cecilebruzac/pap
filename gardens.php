<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>

<div class="gardens">
  <nav class="gardens__nav secondary-nav">
    <div class="row row--centered">
      <div class="secondary-nav__wrapper">
        <div class="secondary-nav__col">
          <h1 class="text text--medium text--bigger-md text--bold">Potagers d’intérieur</h1>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
          <button class="secondary-nav__toggler toggler"
                  onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
                  aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
                     role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
          </button>
        </div><!--
        --><div class="secondary-nav__col secondary-nav__show-md">
          <div class="secondary-nav__menu menu">
            <ul class=" menu__list">
              <li class="menu__item">
                <a class="menu__link menu__link--with-icon"
                   href="/modulo.php">
                  <svg class="menu__link-icon"
                       viewBox="0 0 22 25" width="22" height="25"
                       role="img">
                    <title>Modulo</title>
                    <use xlink:href="assets/images/defs.svg#modulo"></use>
                  </svg>
                  <span class="text text--big">Modulo</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link menu__link--with-icon" href="/lilo.php">
                  <svg class="menu__link-icon"
                       viewBox="0 0 18 21" width="18" height="21"
                       role="img">
                    <title>Lilo</title>
                    <use xlink:href="assets/images/defs.svg#lilo"></use>
                  </svg>
                  <span class="text text--big">Lilo</span>
                </a>
              </li><!--
              --><li class="menu__item">
                <a class="menu__link" href="/comparison-of-gardens.php">
                  <span class="text text--big">Comparaison</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div id="collapsable-secondary-menu"
           class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link"
               href="/modulo.php">
              <span class="text text--big">Modulo</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="/lilo.php">
              <span class="text text--big">Lilo</span>
            </a>
          </li>
          <li class="menu__item">
            <a class="menu__link"
               href="/comparison-of-gardens.php">
              <span class="text text--big">Comparaison</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <section class="gardens__introduction section">
    <picture class="gardens__illustration">
      <source media="(min-width: 50em)"
              sizes="100vw"
              srcset="assets/images/potagers-800.jpg 800w,
							assets/images/potagers-1024.jpg 1024w,
							assets/images/potagers-1600.jpg 1600w,
							assets/images/potagers-2048.jpg 2048w,
							assets/images/potagers-1366.jpg 1366w,
							assets/images/potagers-2732.jpg 2732w">
      <img sizes="100vw"
           srcset="assets/images/potagers-crop-375.jpg 375w,
					 assets/images/potagers-crop-750.jpg 750w,
					 assets/images/potagers-crop-800.jpg 800w,
					 assets/images/potagers-crop-1600.jpg 1600w"
           src="assets/images/potagers-1024.jpg"
           alt="Les potagers d'intérieur"/>
    </picture>
    <div class="row row--centered">
      <ul class="gardens__list">
        <li class="gardens__item row__col row__col--half-sm">
          <article class="gardens__garden garden">
            <a href="/modulo.php">
              <h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">MODULO</h2>
              <h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre potager d’intérieur&nbsp;évolutif</h3>
            </a>
            <p class="garden__price text text--big text--bigger-md">149,95&nbsp;€</p>
            <div class="garden__cta text--big-md">
              <button class="button button--sale">Acheter</button>
            </div>
            <p class="garden__see-more text text--medium">
              <a class="text__link" href="/modulo.php">Découvrir Modulo</a>
            </p>
          </article>
        </li><!--
        --><li class="gardens__item row__col row__col--half-sm">
          <article class="gardens__garden garden">
            <a href="/lilo.php">
              <h2 class="garden__name text text--big text--bigger-sm text--huge-lg text--bold">LILO</h2>
              <h3 class="garden__intro text text--medium text--big-sm text--bigger-lg">Votre jardin à portée
                de&nbsp;main</h3>
            </a>
            <p class="garden__price text text--big text--bigger-md">99,95&nbsp;€</p>
            <div class="garden__cta text--big-md">
              <button class="button button--sale">Acheter</button>
            </div>
            <p class="garden__see-more text text--medium">
              <a class="text__link" href="/lilo.php">Découvrir Lilo</a>
            </p>
          </article>
        </li>
      </ul>
    </div>
  </section>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">
            Comparaison</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Découvrez le potager qui remplit tous vos&nbsp;besoins.</h2>
        </header>
        <p class="text text--medium text--centered"><a class="button" href="/comparison-of-gardens.php">Je compare les&nbsp;potagers</a></p>
      </div>
    </div>
  </section>
  <section class="gardens__exposure section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="section__icon">
            <svg viewBox="0 0 35 35" width="100" height="100"
                 role="img" aria-hidden="true">
              <use xlink:href="assets/images/defs.svg#exposure"></use>
            </svg>
          </p>
          <p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Lumière
            performante et&nbsp;adaptable</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">La lumière du soleil dans votre potager&nbsp;d’intérieur.</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">Le luminaire s’allume automatiquement le matin et
          s‘éteint le soir. Équipé de dizaines de LEDs basse consommation, il sait reproduire la lumière du soleil pour
          répondre aux besoins de vos plantes. En savoir plus sur <a class="text__link"
                                                                     href="/technology.php">notre&nbsp;technologie</a>.</p>
      </div>
    </div>
  </section>
  <?php include 'components/content-sections/simple-and-autonomous-device.php'; ?>
  <section class="section section--with-alt-background-color">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Potager
            d’intérieur</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Un mini jardin potager pour vos aromates,
            légumes et&nbsp;fleurs</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">Avec les mini potagers Lilo et Modulo, découvrez sans
          cesse de nouvelles saveurs pour parfumer tous vos plats et renouveler votre inspiration culinaire. Vous
          disposez de plantes aromatiques ultra-fraîches à tout moment&nbsp;: les recettes du pesto et du thé à la
          menthe n’auront plus de secret pour&nbsp;vous&nbsp;!</p>
        <p class="text text--medium text--big-md text--centered">Fini les plants de basilics qui fanent après 3 jours&nbsp;:
          chaque capsule vous permet d’avoir trois à quatre mois de croissance et de cueillette. Un potager d’intérieur
          Prêt à Pousser, c'est la garantie de belles plantes qui vont durer longtemps et d'herbes aromatiques qui
          releveront tous vos plats du&nbsp;quotidien.</p>
      </div>
    </div>
  </section>
  <div class="gardens__video-and-recipes">
    <section class="section">
      <div class="row row--centered">
        <div class="row__col row__col--centered row__col--three-fifths-md">
          <header class="section__header">
            <p class="text text--medium text--bigger-md text--uppercase text--light text--centered show-md">Dans
              n'importe quel intérieur</p>
            <h2 class="text text--big text--bigger-md text--bold text--centered">Le potager d’appartement pour faire
              pousser des herbes aromatiques facilement et toute&nbsp;l’année</h2>
          </header>
          <p class="text text--medium text--big-md text--centered">Vous n'avez pas la main verte&nbsp;? Pas grave&nbsp;!
            Modulo et Lilo révolutionnent le jardinage d’intérieur et prennent en charge tous les besoins de vos plantes&nbsp;:
            eau, lumière, nutriments… Même en hiver, notre système hydroponique breveté vous permet de cultiver votre
            potager sans balcon et avec un minimum d’entretien&nbsp;: ne vous demandez plus jamais comment faire pousser
            de la&nbsp;menthe.</p>
          <p class="text text--medium text--big-md text--centered">Il n’y a presque rien à faire&nbsp;: plantez, admirez
            et dégustez&nbsp;! Le pot de basilic qui meurt au bout de trois jours et le persil du supermarché qui
            flétrit au fond du frigo appartiennent désormais au&nbsp;passé.</p>
          <p class="video-iframe">
            <iframe title="Lilo"
                    width="560"
                    height="315"
                    src="https://www.youtube.com/embed/B9BB3yOzvIo"
                    frameBorder="0"
                    allow="autoplay; encrypted-media"
                    allowFullScreen></iframe>
          </p>
        </div>
      </div>
    </section>
    <section class="gardens__recipes section">
      <div class="row row--centered">
        <div class="row__col row__col--three-fifths-md row__col--with-right-gutter-md">
          <header class="section__header">
            <p class="text text--big text--bigger-md text--uppercase text--light show-md">Pour cuisiner au quotidien</p>
            <h2 class="text text--big text--bigger-md text--bold">De véritables potagers d‘intérieur pour votre
              appartement</h2>
          </header>
          <p class="text text--medium text--big-md">Pourquoi les aromates Prêt à Pousser sont-ils meilleurs&nbsp;? Car
            vous savez enfin d’où ils viennent et vous les avez fait pousser de A&nbsp;à&nbsp;Z&nbsp;! Alors faites
            entrer la nature et la santé dans votre cuisine avec des fines herbes fraîches et vitaminées qui raviront
            les papilles des petits comme des grands&nbsp;: à cultiver sans&nbsp;modération&nbsp;!</p>
          <p class="text text--medium"><a class="button" href="">Toutes les recettes</a></p>
        </div><!--
        --><div class="row__col row__col--two-fifths-md show-md">
          <img class="gardens__recipes-illustration"
               src="assets/images/assiette.png"
               srcset="assets/images/assiette@2x.png 2x"
               alt="Basilic grand vert"/>
        </div>
      </div>
    </section>
  </div>
  <section class="section">
    <div class="row row--centered">
      <div class="row__col row__col--centered row__col--three-fifths-md">
        <header class="section__header">
          <p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Des plantes en
            hydroponie</p>
          <h2 class="text text--big text--bigger-md text--bold text--centered">Un potager autonome et intelligent qui
            réinvente l’agriculture&nbsp;urbaine</h2>
        </header>
        <p class="text text--medium text--big-md text--centered">Les astronautes utilisent l’hydroponie pour cultiver
          des légumes dans l’espace. Sur Terre, c’est Prêt à Pousser qui s’occupe de bien vous nourrir grâce à sa
          technologie inspirée de la NASA et de l’indoor garden (ou indoor gardening). La culture en hydroponie permet
          d’avoir un mini potager autonome et intelligent dans son appartement, sans traitement chimique ou pesticide.
          Avec Modulo et Lilo, la juste dose d’eau et de lumière (LEDs basse consommation) est apportée à vos plantes
          pour qu’elles grandissent dans les meilleures&nbsp;conditions.</p>
        <p class="text text--medium text--big-md text--centered">Terminé le gaspillage d’eau et d’électricité&nbsp;: ce
          petit potager est bon pour vous et pour la planète. Chez Prêt à Pousser, l’esprit startup nous pousse à viser
          la lune, et au-delà&nbsp;!</p>
        <p class="text text--big text--centered">🚀</p>
      </div>
    </div>
  </section>
</div>

<hr>

<?php include 'components/footer.php'; ?>
