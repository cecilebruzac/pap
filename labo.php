<?php
  include 'components/header.php';
  require_once 'components/main-nav.php';
  $isThereASecondaryNav = true;
  displayMainNav($isThereASecondaryNav);
?>
<div class="labo">
	<nav class="labo__nav secondary-nav">
		<div class="row row--centered">
			<div class="secondary-nav__wrapper">
				<div class="secondary-nav__col">
					<p class="text text--medium text--bigger-md text--bold">Prêt à Pousser</p>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__hide-md">
					<button class="secondary-nav__toggler toggler"
									onclick="toogleMenu(this, 'collapsable-secondary-menu', false)"
									aria-label="Afficher ou masquer le sous menu">
              <span class="toggler__arrow">
                <svg viewBox="0 0 12 8" width="12" height="8"
										 role="img" aria-hidden="true">
                  <use xlink:href="assets/images/defs.svg#arrow-down"></use>
                </svg>
              </span>
					</button>
				</div><!--
        --><div class="secondary-nav__col secondary-nav__show-md">
					<div class="secondary-nav__menu menu">
						<ul class="menu__list">
							<li class="menu__item">
								<a class="menu__link"
									 href="/team.php">
									<span class="text text--big">Qui sommes-nous</span>
								</a>
							</li><!--
							--><li class="menu__item">
								<a class="menu__link" href="/recruitment.php">
									<span class="text text--big">Recrutement</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link menu__link--active menu__link--active-border" href="/labo.php">
									<span class="text text--big">Notre labo R&D</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="#">
									<span class="text text--big">Notre histoire</span>
								</a>
							</li><!--
          		--><li class="menu__item">
								<a class="menu__link" href="/school.php">
									<span class="text text--big">1 Kit 1 École</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="collapsable-secondary-menu"
					 class="secondary-nav__menu secondary-nav__menu--collapsable secondary-nav__hide-md menu">
				<ul class="menu__list">
					<li class="menu__item">
						<a class="menu__link"
							 href="/team.php">
							<span class="text text--big">Qui sommes-nous</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/recruitment.php">
							<span class="text text--big">Recrutement</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link menu__link--active" href="/labo.php">
							<span class="text text--big">Notre labo R&D</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="#">
							<span class="text text--big">Notre histoire</span>
						</a>
					</li>
					<li class="menu__item">
						<a class="menu__link" href="/school.php">
							<span class="text text--big">1 Kit 1 École</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<header class="labo__intro">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<h1 class="labo__title text text--bold text--centered text--big text--huge-md">Notre labo&nbsp;R&D</h1>
				<p class="text text--big text--bigger-md text--centered">Chez Prêt à Pousser, nous avons mis en place un
					programme participatif pour faire découvrir le monde végétal aux&nbsp;écoliers.</p>
			</div>
		</div>
	</header>
	<img class="labo__ill"
			 sizes="100vw"
			 srcset="assets/images/rayons-du-laboratoire-375.jpg 375w,
				 assets/images/rayons-du-laboratoire-750.jpg 750w,
				 assets/images/rayons-du-laboratoire-1024.jpg 1024w,
				 assets/images/rayons-du-laboratoire-2048.jpg 2048w,
				 assets/images/rayons-du-laboratoire-1366.jpg 1366w,
				 assets/images/rayons-du-laboratoire-2732.jpg 2732w"
			 src="assets/images/rayons-du-laboratoire-1024.jpg"
			 alt="Laboratoire"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Le
						laboratoire</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Un espace de 120&#x202f;m<sup>2</sup> au
						service de l’innovation&nbsp;végétale</h2>
				</header>
				<p class="text text--medium text--big-md text--centered">
					Plus de 1500 plantes poussent en continu dans notre laboratoire. L’espace est sous atmosphère controlée&nbsp;:
					la température est maintenue entre 20&#x202f;°C et 25&#x202f;°C tandis que le taux d’humidité est régulé. Ces
					paramètres permettent d’offrir aux plantes des conditions de culture uniformisées sur toute l’année. Les
					expériences végétales sont réparties sur trois différentes salles, chacune avec ses propres caractéristiques.
					Cela nous permet de reproduire tout type d’environnement et de réaliser des expériences abouties. C’est dans
					cet environnement que les plantes sont testées, contrôlées et améliorées afin de vous délivrer la meilleure
					expérience&nbsp;possible.
				</p>
				<p class="text text--medium text--big-md text--centered">&nbsp;</p>
				<p class="video-iframe">
					<iframe title="Lilo"
									width="560"
									height="315"
									src="https://www.youtube.com/embed/rilUuNo9cdA"
									frameBorder="0"
									allow="autoplay; encrypted-media"
									allowFullScreen></iframe>
				</p>
				<p class="text text--medium text--big-md text--centered">Voici Fred, aussi appelé Frédéric Lota. Directeur de la
					Recherche et Développement, il gère l'équipe au quotidien pour améliorer en permanence les variétés
					commercialisées, en développer de nouvelles et concocter tous les nouveaux projets végétaux de Prêt à&nbsp;Pousser. </p>
			</div>
		</div>
	</section>
	<img class="labo__ill"
			 sizes="100vw"
			 srcset="assets/images/tournesols-375.jpg 375w,
				 assets/images/tournesols-750.jpg 750w,
				 assets/images/tournesols-1024.jpg 1024w,
				 assets/images/tournesols-2048.jpg 2048w,
				 assets/images/tournesols-1366.jpg 1366w,
				 assets/images/tournesols-2732.jpg 2732w"
			 src="assets/images/tournesols-1024.jpg"
			 alt="Tournesols"/>
	<section class="section">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered show-md">Nos axes de&nbsp;travail</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">L’innovation de l’indoor gardening
						en 4&nbsp;actes</h2>
				</header>
				<ul class="labo__axes">
					<li class="labo__axe">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">01-AM</h3>
						<p class="text text--medium text--big-md text--centered"><span class="text--bold">Expériences d’amélioration des variétés&nbsp;commercialisées</span><br>Nous
							travaillons sans cesse sur l’amélioration de la gamme actuelle. Parmi <a class="text__link"
																																											 href="/plants.php">toutes les
								variétés de plantes</a> actuellement disponibles, nous continuons de les tester au quotidien pour
							vérifier que le niveau de qualité atteint reste élevé. Nous sommes soucieux de délivrer les meilleures
							plantes,&nbsp;toujours.</p>
					</li>
					<li class="labo__axe">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">02-EXT</h3>
						<p class="text text--medium text--big-md text--centered"><span class="text--bold">Expériences d’extension de&nbsp;plantes</span><br>Nous
							travaillons sans cesse à élargier la gamme actuelle. Déjà plus de 40 variétés de plantes sont
							commercialisées mais nous ne comptons pas nous arrêter là, nous vous savons curieux et nous voulons vous
							faire découvrir des plantes toujours plus incroyables. Fraises, haricots, on n’en dit pas plus mais vous
							allez être radis (ou ravis c’est&nbsp;selon).</p>
					</li>
					<li class="labo__axe">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">03-SIMP</h3>
						<p class="text text--medium text--big-md text--centered"><span class="text--bold">Expériences de simplification sur nos plantes et&nbsp;produits</span><br>Afin
							de vous garantir une expérience toujours plus agréable, nous cherchons continuellement à simplifier la
							façon dont vous utilisez nos produits. Les expériences 03-SIMP ont notamment permi de lancer nos toutes
							nouvelles capsules intégrant directement les nutriments et les graines BIO. Vous n’avez plus qu’à insérer
							la capsule dans votre potager, et&nbsp;voilà&nbsp;!</p>
					</li>
					<li class="labo__axe">
						<h3 class="text text--bold text--centered text--big text--huge-md text--light-grey">04-MOD</h3>
						<p class="text text--medium text--big-md text--centered"><span class="text--bold">Conception du nouveau potager&nbsp;Modulo</span><br>Nous
							préparons depuis des mois l’arrivée de Modulo&nbsp;: sa conception, son nouveau système hydroponique plus
							abouti, les nouveaux designs des capsules permettant d’obtenir des récoltes plus rapides et abondantes,
							ainsi que toutes les nouvelles variétés spécialement conçus pour Modulo. Après de nombreux mois de
							conception et de tests, nous avons enfin décidé de le commercialiser car nous sommes satisfaits de sa
							qualité et de ses performances. Nous pensons que vous allez&nbsp;l’adorer.</p>
					</li>
				</ul>
				<p class="text text--medium text--centered">Découvrez nos dernières expériences et lancements sur la page <a
						class="text__link"
						href="">Notre histoire</a>.</p>
				<p class="text text--medium text--centered"><a class="button" href="">Notre histoire</a></p>
			</div>
		</div>
	</section>
	<hr class="show-md">
	<section class="section labo__gallery show-md">
		<div class="row row--centered">
			<div class="row__col row__col--centered row__col--three-fifths-md">
				<header class="section__header">
					<p class="text text--big text--bigger-md text--uppercase text--light text--centered">Des photos
						de&nbsp;nous</p>
					<h2 class="text text--big text--bigger-md text--bold text--centered">Découvrez l’espace&nbsp;R&D</h2>
				</header>
				<div class="slider-with-arrows">
					<div>
						<img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
								 srcset="assets/images/laboratoire-355.jpg 355w,
								 assets/images/laboratoire-710.jpg 710w,
								 assets/images/laboratoire-677.jpg 677w,
								 assets/images/laboratoire-1354.jpg 1354w"
								 src="assets/images/laboratoire-677.jpg"
								 width="677"
								 alt="Laboratoire"/>
					</div>
					<div>
						<img sizes="(min-width: 50em) calc(60vw - 1.25em), calc(100vw - 1.25em)"
								 srcset="assets/images/laboratoire-355.jpg 355w,
								 assets/images/laboratoire-710.jpg 710w,
								 assets/images/laboratoire-677.jpg 677w,
								 assets/images/laboratoire-1354.jpg 1354w"
								 src="assets/images/laboratoire-677.jpg"
								 width="677"
								 alt="Laboratoire"/>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<hr>
<?php include 'components/footer.php'; ?>
